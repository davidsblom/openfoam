//Moving all points of the ibFaces to the symmetryplane in the direction of the symmetryPlane normal
Info << "Moving points at symmetry plane " << endl;
const labelList& ibFacesExt = isp.ibFacesExt();
const faceList& faces = mesh.faces();
const vectorField& points = mesh.points();
vectorField newPoints(points);

const vectorField& faceNormals = mesh.faceAreas();
const labelList& owners = mesh.owner();
const scalarField& gammaExtTopoI = isp.gammaExtTopo().internalField();

//Check face orientation and switch face points if needed
flipList.setSize(ibFacesExt.size(),false);
label tCounter(0);
forAll(ibFacesExt,iFace)
{
	//Normal direction
	label faceID = ibFacesExt[iFace];
	vector fN = faceNormals[faceID]/(mag(faceNormals[faceID])+SMALL);
	scalar faceOrientation = fN & N0;
	flipList[iFace]=false;//Is set to false at first
	
	//stretchFaceArea(f,newPoints);
	
	if(gammaExtTopoI[owners[faceID]] < SMALL)
	{
		if(faceOrientation > 0)
		{
			const face& f=faces[faceID];
			switchFacePoints(f,newPoints);
			tCounter++;
		}
		flipList[iFace]=true;
	}
	else
	{
		if(faceOrientation < 0)
		{
			const face& f=faces[faceID];
			switchFacePoints(f,newPoints);
			tCounter++;
		}
	}
}

vectorField newOldPoints(oldPoints);//Start with old point location before RBF deformation and construct location at t(n*)
forAll(isp.ibFacePointsExt(),iPoint)
{
	label pointID = isp.ibFacePointsExt()[iPoint];
	newOldPoints[pointID] = newPoints[pointID];//Copy new face location to old mesh to recompute V0 and meshPhi
}

oldPoints = mesh.points();//Save old RBF location to move the mesh back for RBF deformation at later stage

mesh.checkFaceAreas(true);
//Move mesh "back" to old RBF deformed position with the new face location -> get new old cell volumes
scalarField sweptVolumes0 = mesh.movePoints(newOldPoints);//Move to t(n*) and record sweptvolumes

//***************************************************
//Extra checks due to ZERO FACE AREA
mesh.checkFaceAreas(true);
const vectorField& faceNormalsNew = mesh.faceAreas();
const vectorField& faceCentres = mesh.faceCentres();
forAll(faceNormalsNew,faceID)
{
	//Normal direction
//	label faceID = ibFacesExt[iFace];
	if(mag(faceNormalsNew[faceID])<SMALL)
	{
		Info << "face: " << faceID << " has area = " << mag(faceNormalsNew[faceID]) << ", face centre = " << faceCentres[faceID] << endl;
	}
}
//***************************************************

//Check minimum new old volume
scalar minNewOldVolume = min(mesh.V().field());
Info << "Minimum new old Volume = " << minNewOldVolume << endl;
if(minNewOldVolume < SMALL)
{
	FatalErrorIn("icoDyMFoamTopoISP moveSymPoints")
            << "minimum new old volume is to small: "
            << minNewOldVolume
            << abort(FatalError);
}
scalarField oldV = mesh.V0();//Used to calculate statistics

scalarField& newOldV = mesh.setV0().field();
newOldV = mesh.V().field();//Set old volume to new old volume

scalarField sweptVolumes = mesh.movePoints(newPoints);//Move to location of t(n+1)

//Check if faces at symboundary have no volume swept
forAll(isp.ibFacesExt(),iFace)
{
	label faceID = isp.ibFacesExt()[iFace];
	if(mag(sweptVolumes[faceID]-sweptVolumes0[faceID])>SMALL)
	{
		Info << "Difference between sweptvolumes at face " << faceID << " = " << mag(sweptVolumes[faceID] - sweptVolumes0[faceID]) << endl;
	}
}

//Set the new fluxes corresponding to the new volume change in the internalField and to boundaryFields with movingWallVelocity as bc
const scalar& dt = mesh.time().deltaT().value();
forAll(phiMesh.internalField(),iFace)
{
	phiMesh.internalField()[iFace] = (sweptVolumes[iFace] - sweptVolumes0[iFace])/dt;
}

forAll(U.boundaryField(),iPatch)
{
	if(U.boundaryField().types()[iPatch]==word("movingWallVelocity"))
	{
		const polyPatch& pp = mesh.boundaryMesh()[iPatch];
		forAll(pp,iFace)
		{
			label faceID = pp.start()+iFace;
			phiMesh.boundaryField()[iPatch][iFace] = (sweptVolumes[faceID] - sweptVolumes0[faceID])/dt;
		}
	}
}
