const polyPatch& bPatch = mesh.boundaryMesh()[symPatchID];
const scalarField& pSymPatch = p.boundaryField()[symPatchID];
const vectorField& USymPatch = U.boundaryField()[symPatchID];
const vectorField& faceC = mesh.faceCentres();

oldPSymPlane.setSize(bPatch.size());
oldYSymPlane.setSize(bPatch.size());
oldUySymPlane.setSize(bPatch.size());

forAll(bPatch,iFace)
{
	label faceID = bPatch.start() + iFace;
	oldYSymPlane[iFace]=faceC[faceID].y();
	oldPSymPlane[iFace]=pSymPatch[iFace];
	oldUySymPlane[iFace]=USymPatch[iFace].y();
}
