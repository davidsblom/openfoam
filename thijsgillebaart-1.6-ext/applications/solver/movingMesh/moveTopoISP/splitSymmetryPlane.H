//Split at the symmetryPlane
if(zoneID == -1)
{
    faceZones.resize(faceZones.size()+1);
    zoneID = faceZones.size()-1;
}
Info << "aa" << endl;
//Create zone from these faces and add it to list

faceZones.set
(zoneID,
	new faceZone
	(
	word("symPlane"),
	ibFacesExt,
	flipList,
	zoneID,
	faceZones
	)
);
Info << "ab" << endl;
//Split the mesh
polyTopoChanger splitter(mesh);
splitter.setSize(1);
Info << "ac" << endl;
splitter.set
(
    0,
    new attachDetach
    (
        "Splitter",
        0,
        splitter,
        "symPlane",
        "symPlane",
        "symPlaneDead",
        scalarField(1, runTime.value())
    )
);
Info << "ad" << endl;

//mesh.write();
autoPtr<mapPolyMesh> splitterMap = splitter.changeMesh();
Info << "ae" << endl;
