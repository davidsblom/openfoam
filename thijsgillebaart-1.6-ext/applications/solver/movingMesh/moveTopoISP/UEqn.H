    fvVectorMatrix UEqn
    (
        fvm::ddt(U)
      + fvm::div(phi, U)
      - fvm::laplacian(nu, U)
    );

	//Set solution of right side to zero (making sure it is not solved for)
	vectorField UPartialSol(isp.deadCellsTopo().size(),vector(0,0,0));
	UEqn.setValues(isp.deadCellsTopo(),UPartialSol);

    if (momentumPredictor)
    {
    	solve(UEqn == -isp.gammaExtTopo()*fvc::grad(p));
    }
