//Moving all points of the ibFaces to the symmetryplane in the direction of the symmetryPlane normal
Info << "Moving points at symmetry plane " << endl;
const faceList& faces = mesh.faces();
const vectorField& points = mesh.points();
vectorField newPoints(points);

//Calculate position on symmetry plane
forAll(isp.ibFacePointsExt(),iPoint)
{
	label pointID = isp.ibFacePointsExt()[iPoint];
	newPoints[pointID] = points[pointID] + N0*(N0&(R0 - points[pointID]));
}
const vectorField& faceNormals = mesh.faceAreas();
const labelList& owners = mesh.owner();
const scalarField& gammaExtTopoI = isp.gammaExtTopo().internalField();

//Check zero area faces
stretchFaceArea(mesh,newPoints,isp);

//Check face orientation and switch face points if needed
flipList.setSize(ibFacesExt.size(),false);
label tCounter(0);
forAll(ibFacesExt,iFace)
{
	//Normal direction
	label faceID = ibFacesExt[iFace];
	vector fN = faceNormals[faceID]/(mag(faceNormals[faceID])+SMALL);
	scalar faceOrientation = fN & N0;
	flipList[iFace]=false;//Is set to false at first
	
	if(gammaExtTopoI[owners[faceID]] < SMALL)
	{
		if(faceOrientation > 0)
		{
			const face& f=faces[faceID];
			switchFacePoints(f,newPoints);
			tCounter++;
		}
		flipList[iFace]=true;
	}
	else
	{
		if(faceOrientation < 0)
		{
			const face& f=faces[faceID];
			switchFacePoints(f,newPoints);
			tCounter++;
		}
	}
}

sweptVolumes = mesh.movePoints(newPoints);//Move to location of t(n+1)
