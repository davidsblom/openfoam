/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Application
    icoFoam

Description
    Transient solver for incompressible, laminar flow of Newtonian fluids with
    mesh motion.  Set up as a fake fluid structure interaction solver

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "dynamicFvMesh.H"  
#include "BodyMotion.H"
#include "RBFMotionSolverRigid.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
#   include "setRootCase.H"//Set location of case
#   include "createTime.H"//Create runtime environment
#   include "createDynamicFvMesh.H"//Create dynamic fluid mesh
#   include "readBodyProperties.H"//Reading body properties: spring stifnesses, mass properties, cg, rc
#   include "readTimeControls.H"//Read controlDict

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    label nIters = 3;

    while (runTime.run())
    {
#       include "readPISOControls.H"//Read PISO control every new time step
#       include "readTimeControls.H"//Read controlDict every new time step
        
        // New time
        runTime++;

        //Update old values because new time step
        bodyMotion.update();
        vectorField totalMotion(movingPoints.size(),vector(0.0,0.0,0.0));    

        Info<< "Time = " << runTime.timeName() << nl << endl;

        //Test if including iterations
        for(int i=0;i<nIters;i++){
    #       include "solveSolid.H"
            vectorField motion = bodyMotion.getMotion(movingPoints);
            totalMotion += motion;
            //Info << i << ": motion = " << motion << endl;
            
            ms.setMotion(motion);
            
            bool meshChanged = mesh.update();
        }
        //Info << "totalMotion = " << totalMotion << endl;

        //Write bodyMotion properties to file (if requested)
		bodyMotion.write();
        runTime.write();
		
        Info<< "ExecutionTime = "
            << runTime.elapsedCpuTime()
            << " s\n\n" << endl;
    }
    
    Info<< "End\n" << endl;

    return(0);
}


// ************************************************************************* //
