{
	const scalarField& faceAreas = mesh.magSf().boundaryField()[patchID];
	const vectorField& faceC = mesh.boundaryMesh()[patchID].faceCentres();
	scalar totalArea = sum(faceAreas);
	vectorField forces(bodyPatch.size(),vector(0,0,0));
	
	scalar F0 = 10;
	scalar w = 5;
	scalar time = runTime.time().value();
	scalar F = F0*Foam::cos(w*time);
	forAll(bodyPatch,iface){
		//forces[iface] = sign(faceC[iface].y())*vector(1*faceAreas[iface]/totalArea,0,0);
		//forces[iface] = vector(0,1*faceAreas[iface]/totalArea,0);
		forces[iface] = vector(F*faceAreas[iface]/totalArea,0,0);
	}

    bodyMotion.calculatePosition(forces);
}