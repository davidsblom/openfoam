/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Application
    icoFoam

Description
    Transient solver for incompressible, laminar flow of Newtonian fluids with
    mesh motion.  Set up as a fake fluid structure interaction solver

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "dynamicFvMesh.H"
#include "RBFMotionSolverRigid.H"
#include "fsiInterface.H"
#include "BodyMotion.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
#   include "setRootCase.H"//Set location of case
#   include "createTime.H"//Create runtime environment
#   include "createDynamicFvMesh.H"//Create dynamic fluid mesh
#   include "checkTimeDiscretisationSchemeRC.H"
#   include "createFields.H"//Create fluid fields for incompressible flow & read (constant) density (rhoFluid)
#   include "readCouplingProperties.H"//Read coupling patches solid and fluid. Construct interpolators (fluid-solid, solid-fluid), mesh motion object (tForce, motionUFluidPatch, tppi)
#   include "readBodyProperties.H"//Reading body properties: spring stifnesses, mass properties, cg, rc
#   include "readTimeControls.H"//Read controlDict

#   include "initContinuityErrs.H"//Initialize continuity error calculations

    fsiInter.setNormValue(rhoFluid.value());
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
#       include "readControls.H"//Read PISO control every new time step
#       include "CourantNo.H"//Calculate Co

        //TEMP: store old Sf for ddtPhiCorr
        oldoldSf == oldSf;
        oldSf == mesh.Sf();

        // Make the fluxes absolute
        fvc::makeAbsolute(phi, U);
        
#       include "setDeltaT.H"//Set dT according to Co if requested

        // New time
        runTime++;

        //Update old values because new time step
        bodyMotion.update();

        Info<< "Time = " << runTime.timeName() << nl << endl;

		//Re-initialize variables each time step
		fsiInter.reset();
		
		//Field values
		p_prevIter = p;//store initial p;
		
		do{

        	Info<< "Fsi iteration = " << fsiInter.iter() << endl;
			
            //p = p_prevIter;//Set pressure field back to p at tn for fluid calculations
            
            if(fsiInter.iter() != 0){
                fvc::makeAbsolute(phi, U);//in solveFluid makeRelative is called. First absolute flux are needed to calculate the correct flux again based on the new position
            }       

            bodyMotion.calculatePosition(fsiInter.getForces());//Solving solid motion
	#       include "setMotion.H"
            
            if (correctPhi && (mesh.moving() || meshChanged))
            {
                // Fluxes will be corrected to absolute velocity
                // HJ, 6/Feb/2009
    #           include "correctPhi.H"
            }

            // Make the fluxes relative
            fvc::makeRelative(phi, U);

	#       include "solveFluid.H"
			
			fsiInter.update(rhoFluid,nu,U,p);
		} while(!fsiInter.converged());
		
		Info << "Number of fsi coupling iterations needed: " << fsiInter.iter() << endl;

		//Writing data
        runTime.write();
        //Write bodyMotion properties to file (if requested)
		bodyMotion.write();
		//Write fsi properties to file (if requested)
		fsiInter.write();
		
        Info<< "ExecutionTime = "
            << runTime.elapsedCpuTime()
            << " s\n\n" << endl;
    }
    
    Info<< "End\n" << endl;

    return(0);
}


// ************************************************************************* //
