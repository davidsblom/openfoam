    // Setting mesh motion
	vectorField motion(movingPoints.size(),vector::zero);
	motion = bodyMotion.getMotion(movingPoints);
	ms.setMotion(motion);

    bool meshChanged = mesh.update();//Displaces the fluid mesh based on the motionU on the fluid patch which is just interpolated above

#	include "checkVolContinuity.H"//Check moving mesh continuity errors