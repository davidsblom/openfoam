//SwitchPoints
void switchFacePoints(const face& f,vectorField& points);

void switchFacePoints(const face& f,vectorField& points)
{
	vectorField facePoints(f.size());
	label minZind = 0;
	forAll(f,pointI)
	{
		facePoints[pointI] = points[f[pointI]];
		if(mag(facePoints[0].z() - facePoints[pointI].z()) < SMALL && pointI>minZind)
		{
			minZind = pointI;
		}
	}
	if(minZind==1)
	{
		points[f[0]]=facePoints[1];
		points[f[1]]=facePoints[0];
		points[f[2]]=facePoints[3];
		points[f[3]]=facePoints[2];
	}
	else if(minZind==2)
	{
		points[f[0]]=facePoints[2];
		points[f[2]]=facePoints[0];
		points[f[1]]=facePoints[3];
		points[f[3]]=facePoints[1];
	}
	else if(minZind==3)
	{
		points[f[0]]=facePoints[3];
		points[f[3]]=facePoints[0];
		points[f[1]]=facePoints[2];
		points[f[2]]=facePoints[1];			
	}
	else
	{
	        FatalErrorIn("TGicoDyMFoamISP::switchFacePoints()")
        << "point not found are more than 4 points in face"
        << abort(FatalError);
	}
}
