// Solve the Momentum equation

tmp<fvVectorMatrix> UEqn
(
    fvm::ddt(U)
  + fvm::div(phi, U)
  + turbulence->divDevReff(U)
);

if (oCorr == nOuterCorr-1)
{
    UEqn().relax(1);
}
else
{
    UEqn().relax();
}

if (oCorr == nOuterCorr-1)
{
    solve(UEqn() == -fvc::grad(p), mesh.solver("UFinal"));
}
else
{
    solve(UEqn() == -fvc::grad(p));
}