volVectorField HU = UEqn().H();
volScalarField AU = UEqn().A();
U = HU/AU;
if (nCorr <= 1)
{
    UEqn.clear();
}

phi = ((fvc::interpolate(HU)/fvc::interpolate(AU)) & mesh.Sf()) + fvc::ddtPhiCorr(1.0/AU, U, phi);
forAll(phi.boundaryField(),iPatch)
{
    if(!phi.boundaryField()[iPatch].coupled())
    {
        phi.boundaryField()[iPatch] = U.boundaryField()[iPatch] & mesh.Sf().boundaryField()[iPatch];
    }
}

adjustPhi(phi, U, p);

// Non-orthogonal pressure corrector loop
for (int nonOrth=0; nonOrth<=nNonOrthCorr; nonOrth++)
{
    // Pressure corrector
    fvScalarMatrix pEqn
    (
        fvm::laplacian(1.0/fvc::interpolate(AU), p,"laplacian((1|A(U)),p)") == fvc::div(phi)
    );

    pEqn.setReference(pRefCell, pRefValue);

    if
    (
        oCorr == nOuterCorr - 1
     && corr == nCorr - 1
     && nonOrth == nNonOrthCorr
    )
    {
        pEqn.solve(mesh.solver("pFinal"));
    }
    else
    {
        pEqn.solve();
    }

    if (nonOrth == nNonOrthCorr)
    {
        phi -= pEqn.flux();
    }
}

#include "continuityErrs.H"

// Explicitly relax pressure for momentum corrector except for last corrector
if (oCorr != nOuterCorr-1)
{
    p.relax();
}

U -= (1.0/AU)*fvc::grad(p);
U.correctBoundaryConditions();
