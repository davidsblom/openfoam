
    IOdictionary ispDict
    (
        IOobject
        (
            "immersedSymmetryPlaneProperties",
            runTime.constant(),
            mesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

	// Read what options are switched on
	bool projection(readBool(ispDict.lookup("projection")));
    bool redefinition(readBool(ispDict.lookup("redefinition")));
    bool symmetryPlane(readBool(ispDict.lookup("symmetryPlane")));
    if(symmetryPlane){
        redefinition = true;
    }
    if(redefinition){
        projection = true;
    }

    // Read position of the symmetry plane
    vector R0(ispDict.lookup("R0"));

    // Read normal vector to the symmetry plane
    vector N0(ispDict.lookup("N0"));

    // Read interpolation method
    word interpolationMethod(ispDict.lookup("interpolationMethod"));
    
    bool useGradient = false;
    bool usePoly = false;
    if(interpolationMethod=="RBF"){
		useGradient = ispDict.lookupOrDefault("useGradient",false);
		usePoly = ispDict.lookupOrDefault("usePoly",false);
	}

    immersedSymmetryPlaneTopo isp(mesh, R0, N0,interpolationMethod,useGradient,usePoly);

    if(symmetryPlane){
        isp.gammaExt().write();
        isp.gamma().write();
    }
