//Not sure about this->not needed because phi is automatically adjusted?
//Set the new fluxes corresponding to the new volume change in the internalField and to boundaryFields with movingWallVelocity as bc
Info << "Set phiMesh ----->";
const scalar& dt = mesh.time().deltaT().value();
forAll(phiMesh.internalField(),iFace)
{
	phiMesh.internalField()[iFace] = (sweptVolumes[iFace]-sweptVolumes0[iFace])/dt;
}

forAll(U.boundaryField(),iPatch)
{
	if(U.boundaryField().types()[iPatch]==word("movingWallVelocity"))
	{
		const polyPatch& pp = mesh.boundaryMesh()[iPatch];
		forAll(pp,iFace)
		{
			label faceID = pp.start()+iFace;
			phiMesh.boundaryField()[iPatch][iFace] = (sweptVolumes[faceID]-sweptVolumes0[faceID])/dt;
		}
	}
}
Info << "done" << endl;