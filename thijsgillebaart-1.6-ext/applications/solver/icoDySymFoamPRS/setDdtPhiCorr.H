dimensionedScalar rDeltaT = 1.0/mesh.time().deltaT();

IOobject ddtIOobject
(
    "ddtPhiCorr(" + AU.name() + ',' + U.name() + ',' + phi.name() + ')',
    mesh.time().timeName(),
    mesh
);

// === Set boundaries correct of U === //
surfaceScalarField ddtPhiCoeff
(
    IOobject
    (
        "ddtPhiCoeff",
        mesh.time().timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::NO_WRITE
    ),
    mesh,
    dimensioned<scalar>("1", dimless, 1.0)
);

forAll (U.boundaryField(), patchI)
{
    ddtPhiCoeff.boundaryField()[patchI] = 0.0;
}


//tmp<surfaceScalarField> phiCorr =
//    phi.oldTime() - (fvc::interpolate(U.oldTime()) & mesh.Sf());//TEMP: Old implementation
//tmp<surfaceScalarField> phiCorr =
//    phi.oldTime() - (fvc::interpolate(U.oldTime()) & oldSf);//TEMP: Everything should be old including Sf (Paper tukovic about Rhie-Chow interpolation)
surfaceScalarField phiCorr =
    phi.oldTime() - (fvc::interpolate(U.oldTime()) & oldSf);//TEMP: Everything should be old including Sf (Paper tukovic about Rhie-Chow interpolation)

// === make sure fvc::interpolate is performed on 1.0/rA seperately === //
tmp<surfaceScalarField> ddtPhiCorrLocal = tmp<surfaceScalarField>
(
    new surfaceScalarField
    (
        ddtIOobject,
        rDeltaT*ddtPhiCoeff
       *1.0/fvc::interpolate(AU)*phiCorr
    )
);
