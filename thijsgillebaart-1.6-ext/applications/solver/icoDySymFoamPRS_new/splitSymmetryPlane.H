//Split at the symmetryPlane
if(zoneID == -1)
{
    faceZones.resize(faceZones.size()+1);
    zoneID = faceZones.size()-1;
}

//Create zone from these faces and add it to list

faceZones.set
(zoneID,
	new faceZone
	(
	word(symPatchName),
	ibFacesExt,
	flipList,
	zoneID,
	faceZones
	)
);

//Split the mesh
polyTopoChanger splitter(mesh);
splitter.setSize(1);

splitter.set
(
    0,
    new attachDetach
    (
        "Splitter",
        0,
        splitter,
       	symPatchName,
        symPatchName,
        symPatchName+"Dead",
        scalarField(1, runTime.value())
    )
);

autoPtr<mapPolyMesh> splitterMap = splitter.changeMesh();