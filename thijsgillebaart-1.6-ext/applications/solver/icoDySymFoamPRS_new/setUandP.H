//Interpolate old p and Uy values to the new faces
if(oldYSymPlane.size()>0)
{
	const polyPatch& newSymPatch = mesh.boundaryMesh()[symPatchID];
	const vectorField& newFaceC = mesh.faceCentres();
	vectorField& USymPatch = U.boundaryField()[symPatchID];
	vectorField& UoldSymPatch = U.oldTime().boundaryField()[symPatchID];
	scalarField& pSymPatch = p.boundaryField()[symPatchID];
	
	scalarField newYSymPlane(newSymPatch.size());
	scalarField newPSymPlane(newSymPatch.size());
	scalarField newUySymPlane(newSymPatch.size());
	forAll(newSymPatch,iFace)
	{
		label faceID = newSymPatch.start() + iFace;
		newYSymPlane[iFace] = newFaceC[faceID].y();
	}
	newPSymPlane = interpolateXY(newYSymPlane,oldYSymPlane,oldPSymPlane);
	newUySymPlane = interpolateXY(newYSymPlane,oldYSymPlane,oldUySymPlane);
	
	forAll(newPSymPlane,iFace)
	{
		const label& faceCell = mesh.boundaryMesh()[symPatchID].faceCells()[iFace];
		if(newGamma[faceCell] - oldGamma[faceCell] > (1.0 - SMALL))
		{
			pSymPatch[iFace] = newPSymPlane[iFace];
			USymPatch[iFace].y() = newUySymPlane[iFace];
			UoldSymPatch[iFace].y() = newUySymPlane[iFace];
		}
	}
}