if(redefinition){
	Info << "Redefine old mesh ---->";
	vectorField newOldPoints(oldRbfPoints);//Start with old point location at t(n) (before snapping with RBF from previous time step_)
	forAll(isp.ibFacePointsExt(),iPoint)
	{
		label pointID = isp.ibFacePointsExt()[iPoint];
		newOldPoints[pointID] = newPoints[pointID];//Copy new face location to old mesh to recompute V0 and meshPhi
	}
	
	//Move mesh "back" to old RBF deformed position with the new face location -> get new old cell volumes
	sweptVolumes0 = mesh.movePoints(newOldPoints);//Move to t(n*) and record sweptvolumes

	//Check minimum new old volume
	scalar minNewOldVolume = min(mesh.V().field());
	if(minNewOldVolume < SMALL)
	{
		FatalErrorIn("icoDySymFoamPRS moveSymPoints")
		        << "minimum new old volume is to small: "
		        << minNewOldVolume
		        << abort(FatalError);
	}

	scalarField& newOldV = mesh.setV0().field();
	newOldV = mesh.V().field();//Set old volume to new old volume

	//oldSf is redefined. This is used for correct RhieChow with moving meshes (Tukovic paper)
	//oldSf == mesh.Sf();

	Info << "done" << endl;
}