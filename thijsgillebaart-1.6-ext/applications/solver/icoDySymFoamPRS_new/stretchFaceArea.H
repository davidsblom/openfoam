//SwitchPoints
void stretchFaceArea(const face& f,vectorField& points,const immersedSymmetryPlaneTopo& isp);

void stretchFaceArea(const polyMesh& mesh,vectorField& points,const immersedSymmetryPlaneTopo& isp)
{
	//Mesh characteristics
	const edgeList& edges = mesh.edges();
	const labelListList& faceEdgesList = mesh.faceEdges();
	const labelListList& edgeFaces = mesh.edgeFaces();
	const vectorField& faceCentres = mesh.faceCentres();

	//isp characteristics
	const labelList& ibFacesExt = isp.ibFacesExt();
	const surfaceScalarField& sGammaExt = isp.sGammaExt();
	
	//scaling term
	const scalar moveFactor = 0.0001;
	
	forAll(ibFacesExt,iFace)
	{
		label faceID = ibFacesExt[iFace];
		const labelList& faceEdges = faceEdgesList[faceID];
		forAll(faceEdges,iEdge)
		{
			label edgeID = faceEdges[iEdge];
			label edgeIDmirror = faceEdges[iEdge+2];
			scalar edgeLength = mag(points[edges[edgeID][0]]-points[edges[edgeID][1]]);
			if(edgeLength < SMALL)
			{
				scalar brotherEdgeLength = mag(points[edges[edgeIDmirror][0]]-points[edges[edgeIDmirror][1]]);
				Info << "Edge of face " << faceID << " has zero length! (Brother edge length = "<< brotherEdgeLength << "). Correction is applied :)" <<endl;
				
				//***************************************************************************
				//In 2D internal faces are always 4 point faces -> this is used in the method!
				//2nd assumption: symmetry plane is in y direction (points moved in y dir)
				//***************************************************************************
				forAll(edgeFaces[faceEdges[iEdge+1]],iEdgeFace)
				{
					const label& edgeFaceID = edgeFaces[faceEdges[iEdge+1]][iEdgeFace];
					if(sGammaExt[edgeFaceID] < SMALL && edgeFaceID != faceID)
					{
						const label& pointID0 = edges[faceEdges[iEdge+1]][0];
						const label& pointID1 = edges[faceEdges[iEdge+1]][1];
						Info << "Old point = " << points[pointID0] << "(" << points[pointID1]<<")";
						points[pointID0].y() = points[pointID0].y() + moveFactor*(faceCentres[edgeFaceID].y() - points[pointID0].y());
						points[pointID1].y() = points[pointID1].y() + moveFactor*(faceCentres[edgeFaceID].y() - points[pointID1].y());
						Info << ", new point = " << points[pointID0] << "(" << points[pointID1]<<")" << endl;
					}
				}
				
				label mirrorCorrectionEdgeID = iEdge+3;
				if(mirrorCorrectionEdgeID >= faceEdges.size())
				{
					mirrorCorrectionEdgeID = 0;
				}
				forAll(edgeFaces[faceEdges[mirrorCorrectionEdgeID]],iEdgeFace)
				{
					const label& edgeFaceID = edgeFaces[faceEdges[mirrorCorrectionEdgeID]][iEdgeFace];
					
					if(sGammaExt[edgeFaceID] < SMALL && edgeFaceID != faceID)
					{
						const label& pointID0 = edges[faceEdges[mirrorCorrectionEdgeID]][0];
						const label& pointID1 = edges[faceEdges[mirrorCorrectionEdgeID]][1];
						points[pointID0].y() = points[pointID0].y() + moveFactor*(faceCentres[edgeFaceID].y() - points[pointID0].y());
						points[pointID1].y() = points[pointID1].y() + moveFactor*(faceCentres[edgeFaceID].y() - points[pointID1].y());
					}
				}
				break;
			}
		}
	}
}
