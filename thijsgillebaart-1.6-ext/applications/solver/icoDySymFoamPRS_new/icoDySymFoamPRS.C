/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Application
    icoDyMFoam

Description
    Transient solver for incompressible, laminar flow of Newtonian fluids
    with dynamic mesh.

Author
    Hrvoje Jasak, Wikki Ltd.  All rights reserved.

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "dynamicFvMesh.H"

#include "immersedSymmetryPlaneTopo.H"
#include "switchFacePoints.H"
#include "stretchFaceArea.H"
#include "motionSolver.H"

#include "polyTopoChanger.H"
#include "directTopoChange.H"
#include "polyModifyFace.H"
#include "attachDetach.H"
#include "perfectInterface.H"

#include "interpolateXY.H"
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
#   include "setRootCase.H"
#   include "createTime.H"
#   include "createDynamicFvMesh.H"
#   include "createImmersedSymmetryPlaneTopo.H"
#	include "checkTimeDiscretisationSchemeRC.H"
#	include "createGlobalSymmetryVariables.H"
//#	include "prepareInitialMesh.H"

#   include "initContinuityErrs.H"
#   include "initTotalVolume.H"
#   include "createFields.H"


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;
    while (runTime.run())
    {
#       include "readControls.H"
#       include "checkTotalVolume.H"
#       include "CourantNoU.H"
#       include "setDeltaT.H"

		//Store old Sf for ddtPhiCorr
		oldSf == mesh.Sf();	

		// Make the fluxes absolute
        phi += phiMesh;
		
        runTime++;
        Info<< "Time = " << runTime.timeName() << nl << endl;

		//If not first step (and thus oldPoints>0) move the points back to original position before projection
		if(projection && oldRbfPoints.size()>0)
		{			    
		    if(symmetryPlane){
		    	//Stich the mesh
		    	#include "saveUandP.H"//TEMP
				#include "stitchSymmetryPlane.H"
			}

			//Move points back to previous RBF location
			mesh.movePoints(rbfPoints);
		}

		// ========= Move mesh ========= //
		ms.solve();
		oldRbfPoints = rbfPoints;//Save RBF location to use in next time step as initial redefinition of old mesh
		rbfPoints = ms.curPoints();//Calculate points according to body motion (RBF)
		sweptVolumes = mesh.movePoints(rbfPoints);//Move points to RBF location
		isp.movePoints(mesh.points());//Reset isp

		// ========= Move faces to symmetryPlane ======= //
		const labelList& ibFacesExt = isp.ibFacesExt();//Get isp faces
		if(projection){
			#include "calculateSymPoints.H"//Calculate new face point location of symmetryPlane
			
			//Redefine old mesh
			scalarField oldV = mesh.V0();//TEMP: saving for statistic calculation
			if(redefinition){
				#include "redefineOldMesh.H"
			}

			#include "moveSymPoints.H"//Move face points to symmetryPlane

			if(redefinition){
				scalar maxVoldChange = max(1+(mesh.V0()-oldV)/oldV);
				scalar minVoldChange = min(1+(mesh.V0()-oldV)/oldV);

				//Info << "maxVoldChange = " << maxVoldChange << endl;
				//Info << "minVoldChange = " << minVoldChange << endl;
			}
		}
		
		// === Set phiMesh to (sweptVolumesRBF+sweptVolumes)/dt === //
		#include "setPhiMesh.H"
		
		// ========= Split mesh ========= //
		Info << "Get ibCellsTopo ---->";
		const labelList& ibCellsTopo = isp.ibCellsTopo();//Calculate this value before domain gets split for new cell interpolation
		Info << "done"<<endl;
		if(symmetryPlane){
			Info << "Split symmetry plane ---->";
			#include "splitSymmetryPlane.H" //Split the mesh at the symmetryPlane
			Info << "done" << endl;
		}

		if(symmetryPlane){
			const scalarField& newGamma = isp.gammaExtTopo().internalField();
			const scalarField& oldGamma = isp.gammaExtTopoOld().internalField();
			#include "setUandP.H" //Interpolate old U and p face values to new location
			#include "setValuesForNewIbCells.H"

			//Set boundary values correct for new cells
			p.correctBoundaryConditions();
			U.correctBoundaryConditions();
			
			phi = (fvc::interpolate(U) & mesh.Sf());
 			//Set fluxes of boundary correct
	        forAll(phi.boundaryField(),iPatch)
			{
				if(!phi.boundaryField()[iPatch].coupled())
				{
					phi.boundaryField()[iPatch] = U.boundaryField()[iPatch] & mesh.Sf().boundaryField()[iPatch];
				}
			}
		}		

		#include "checkVolContinuity.H"

		// Make the fluxes relative to the mesh motion
		phi -= phiMesh;//        fvc::makeRelative(phi, U);

		//Outer correction loops needed for correct order convergence -> pimplefoam like
		for (int outCorr=0; outCorr<nOuterCorr; outCorr++)
		{
	#       include "UEqn.H"

		    // --- PISO loop
		    for (int corr=0; corr<nCorr; corr++)
		    {
			    HU = UEqn.H();
			    AU = UEqn.A();

		        U = HU/AU;

		        // ==== Two important steps compard to icoFoamRC: make phi absolute and setDdtPhiCorr using old Sf (manually for EulerRC) ==== //
		        phi += phiMesh;//fvc::makeAbsolute(phi, U);
		        
		        //Calculate pressure gradient free fluxes (non-divergence free)
				phi = ((fvc::interpolate(HU)/fvc::interpolate(AU)) & mesh.Sf());

		        //Writing local ddtPhiCorr only for EulerRC & backwardRC. Using oldSf and oldoldSf instead of current Sf see paper tukovic
#				include "setDdtPhiCorr.H"
		        phi = phi + ddtPhiCorrLocal;
		       	
		        //Set fluxes of boundary correct
		        forAll(phi.boundaryField(),iPatch)
				{
					if(!phi.boundaryField()[iPatch].coupled())
					{
						phi.boundaryField()[iPatch] = U.boundaryField()[iPatch] & mesh.Sf().boundaryField()[iPatch];
					}
				}
		       	// ==== End of two important steps ==== //
		     	
		        //adjustPhi(phi, U, p);

		        for (int nonOrth=0; nonOrth<=nNonOrthCorr; nonOrth++)
		        {
		            fvScalarMatrix pEqn
		            (
		                fvm::laplacian(1.0/fvc::interpolate(AU), p,"laplacian((1|A(U)),p)") == fvc::div(phi)
		            );

		            if(symmetryPlane){
						 //Setting solution of the right part to zero
						scalarField pPartialSol(isp.deadCellsTopo().size(),0.0);
						pEqn.setValues(isp.deadCellsTopo(),pPartialSol);
					}

		            pEqn.setReference(pRefCell, pRefValue);
		            pEqn.solve(mesh.solver(p.name()));

		            if (nonOrth == nNonOrthCorr)
		            {
		                phi -= pEqn.flux();
		            }
		        }

	#           include "continuityErrs.H"

		        // Make the fluxes relative to the mesh motion
		        phi -= phiMesh;//fvc::makeRelative(phi, U);

				if(symmetryPlane){
					 U -= isp.gammaExtTopo()*1.0/AU*fvc::grad(p);
				}else{
					U -= 1.0/AU*fvc::grad(p);
				}
		        U.correctBoundaryConditions();
		    }
		}
        runTime.write();

        Info<< "ExecutionTime = " << runTime.elapsedCpuTime() << " s"
            << "  ClockTime = " << runTime.elapsedClockTime() << " s"
            << nl << endl;
    }

    Info<< "End\n" << endl;

    return(0);
}


// ************************************************************************* //
