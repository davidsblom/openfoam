Info << "Stitching symmetry planes " << endl;
// Master patch
const polyPatch& masterPatch = mesh.boundaryMesh()[mesh.boundaryMesh().findPatchID(symPatchName)];
labelList masterFaces(masterPatch.size());
forAll (masterFaces, i)
{
	masterFaces[i] = masterPatch.start() + i;
}

faceZones.set
(zoneID,
	new faceZone
	(
	word(symPatchName),
	masterFaces,
	flipList,
	zoneID,
	faceZones
	)
);

polyTopoChanger stitcher(mesh);
stitcher.setSize(1);
stitcher.set
(
    0,
    new perfectInterface
    (
        "couple",
        0,
        stitcher,
        symPatchName,
        symPatchName,
        symPatchName+"Dead"
    )
);


//Change the mesh
autoPtr<mapPolyMesh> morphMap = stitcher.changeMesh();
//mesh.movePoints(morphMap->preMotionPoints());
