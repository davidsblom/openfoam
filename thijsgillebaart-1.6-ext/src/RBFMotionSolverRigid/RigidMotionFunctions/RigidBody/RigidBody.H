/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    RigidBody

Description
	Rigid body motion based sinus functions

Author
    Thijs Gillebaart, TU Delft.  All rights reserved.

SourceFiles
    RigidBody.C

\*---------------------------------------------------------------------------*/

#ifndef RigidBody_H
#define RigidBody_H

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "RigidMotionFunction.H"
#include "dictionary.H"
#include "vector.H"
#include "vectorField.H"

namespace Foam
{

/*---------------------------------------------------------------------------*\
                      Class RigidBody Declaration
\*---------------------------------------------------------------------------*/

class RigidBody
:
	public RigidMotionFunction
{
		// Private data

		const dictionary& dict_;
		const polyMesh& mesh_;
		scalar rotationAmplitude_;
		scalar rotationFrequency_;
		scalar translationAmplitude_;
		scalar translationFrequency_;
		vector translationDirection_;
		scalar translationPhaseShift_;
		vector rotationOrigin_;
		scalar phaseShift_;
		bool isSymmetricMotion_;
		dictionary symmetricMotionDict_;
        bool nopatch_;
        vectorField initialPoints_;
        label nPoints_;
	    
    	//- Disallow default bitwise copy construct
        RigidBody(const RigidBody&);

        //- Set initial points when no patch is attached to body
        void setInitialPoints(dictionary dict);
	    
	public:

    //- Runtime type information
    TypeName("RigidBody");

	//Public data

    // Constructors

        //- Construct from dict + mesh
		RigidBody(const dictionary& dict,const polyMesh& mesh);

		virtual autoPtr<RigidMotionFunction> clone() const
        {
            return autoPtr<RigidMotionFunction>(new RigidBody(this->dict_,this->mesh_));
        };

    // Destructor

        virtual ~RigidBody();

    // Member Functions
    
		virtual void getPosition
		(
			const scalar& time,
			const vectorField& movingPoints,
			vectorField& coordinates
		) const;
		
        virtual void getOldPosition
        (
            const vectorField& movingPoints,
            vectorField& coordinates
        ) const;
		
        virtual label nPoints() const;
        virtual const vectorField& initialPoints() const;
		virtual const bool& moveToInitPosition() const;
};
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
