#include "RigidBody.H"
#include "mathematicalConstants.H"
#include "tensor.H"
#include "addToRunTimeSelectionTable.H"
#include "Time.H"

using namespace Foam;

defineTypeNameAndDebug(RigidBody, 0);
addToRunTimeSelectionTable(RigidMotionFunction, RigidBody, dictionary);


// * * * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * * * //
void RigidBody::setInitialPoints(dictionary dict){
	scalar length(readScalar(dict.lookup("length")));
	vector bodydir(dict.lookup("bodyDirection"));
	bodydir = bodydir/mag(bodydir);

	initialPoints_.setSize(2*nPoints_);
	scalar dLength = length/(nPoints_-1+SMALL);
	for(int i=0;i<nPoints_;i++)
	{
		initialPoints_[i]=rotationOrigin_ + bodydir*(i*dLength);
	}

	//Calculate for the rotation the extra control points needed
    vector normal(-(initialPoints_[0].y()-initialPoints_[nPoints_-1].y()),initialPoints_[0].x()-initialPoints_[nPoints_-1].x(),0);
    normal=normal/mag(normal);
    for(int i=0;i<nPoints_;i++)
    {
        initialPoints_[i+nPoints_] = initialPoints_[i]+normal*0.01*length;
    }
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

RigidBody::RigidBody(const dictionary& dict,const polyMesh& mesh):
dict_(dict),
mesh_(mesh),
rotationAmplitude_(readScalar(dict.lookup("rotationAmplitude"))),
rotationFrequency_(readScalar(dict.lookup("rotationFrequency"))),
translationAmplitude_(readScalar(dict.lookup("translationAmplitude"))),
translationFrequency_(readScalar(dict.lookup("translationFrequency"))),
translationDirection_(dict.lookup("translationDirection")),
translationPhaseShift_(0),
rotationOrigin_(dict.lookup("rotationOrigin")),
phaseShift_(0),
isSymmetricMotion_(dict.isDict("symmetricMotion")),
symmetricMotionDict_(),
nopatch_(false),
initialPoints_(0,vector::zero),
nPoints_(0)
{
	translationDirection_ /= mag(translationDirection_) + SMALL;
	rotationAmplitude_ = rotationAmplitude_/180*mathematicalConstant::pi;
	if(dict.found("translationPhaseShift"))
	{
		//Asume shift to be in degrees
		translationPhaseShift_ = readScalar(dict.lookup("translationPhaseShift"))/180*mathematicalConstant::pi;
	}
	if(dict.found("phaseShift"))
	{
		phaseShift_ = readScalar(dict.lookup("phaseShift"))/180*mathematicalConstant::pi;
	}

	nopatch_ = dict.lookupOrDefault("nopatch",false);
	if(nopatch_){
		nPoints_ = readLabel(dict.lookup("nPoints"));
		setInitialPoints(dict);
	}
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

RigidBody::~RigidBody()
{

}

// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void RigidBody::getPosition(const scalar& time,const vectorField& movingPoints,vectorField& coordinates) const
{
	const scalar& pi = mathematicalConstant::pi;

	scalar rotAngle = rotationAmplitude_*sin(2*pi*rotationFrequency_*time+translationPhaseShift_+phaseShift_);
	    
	vector translationVector =
	    translationAmplitude_*sin(2*pi*translationFrequency_*time+translationPhaseShift_)*translationDirection_;

	tensor RzCur(cos(rotAngle), -sin(rotAngle), 0, sin(rotAngle), cos(rotAngle), 0, 0, 0, 1);

	if(nopatch_){
		vectorField rotationField = (RzCur & ( initialPoints_ - rotationOrigin_ )) + rotationOrigin_;
		coordinates = translationVector + rotationField;
	}else{
		vectorField rotationField = (RzCur & ( movingPoints - rotationOrigin_ )) + rotationOrigin_;
		coordinates = translationVector + rotationField;
	}
}

void RigidBody::getOldPosition(const vectorField& movingPoints,vectorField& coordinates) const
{
	const scalar oldTime = mesh_.time().value() - mesh_.time().deltaT().value();
	getPosition(oldTime,movingPoints,coordinates);//Sets coordinates to values at t-dt
}

label RigidBody::nPoints() const
{
	/*FatalErrorIn("label RigidBody::nPoints()")
			    << "Number of moving patches is probably 0 -> not possible for RigidBody type of RigidMotionFunction"
			    << abort(FatalError);*/
	return nPoints_;
}

const vectorField& RigidBody::initialPoints() const
{
	/*FatalErrorIn("label RigidBody::initialPoints()")
			    << "Number of moving patches is probably 0 -> not possible for RigidBody type of RigidMotionFunction"
			    << abort(FatalError);
	vectorField dummy(0,vector::zero);
	return dummy;*/
	return initialPoints_;
}

const bool& RigidBody::moveToInitPosition() const
{
	/*FatalErrorIn("label RigidBody::moveToInitPosition()")
			    << "Number of moving patches is probably 0 -> not possible for RigidBody type of RigidMotionFunction"
			    << abort(FatalError);*/
	bool dummy = false;
	return dummy;
}
