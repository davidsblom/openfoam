/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    RigidFlap

Description
	Rigid motion of flap based on 2 possible rotation axis: top and bottom

Author
    Thijs Gillebaart, TU Delft.  All rights reserved.

SourceFiles
    RigidFlap.C

\*---------------------------------------------------------------------------*/

#ifndef RigidFlap_H
#define RigidFlap_H

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "RigidMotionFunction.H"
#include "dictionary.H"
#include "vector.H"
#include "vectorField.H"

namespace Foam
{

/*---------------------------------------------------------------------------*\
                      Class RigidFlap Declaration
\*---------------------------------------------------------------------------*/

class RigidFlap
:
	public RigidMotionFunction
{
		// Private data
        //Useful references
		const dictionary& dict_;
		const polyMesh& mesh_;

        //Motion variables
		scalar rotationAmplitude_;//Amplitude of motion
		scalar rotationFrequency_;//Frequency of motion
		vector rotationOriginTop_;//Origin for rotation around top parts
        vector rotationAxesTop_;//Axes to rotate about if rotation is done around top (for positive defelction up)
        vector rotationOriginBottom_;//Origin for rotation around bottom parts
        vector rotationAxesBottom_;//Axes to rotate about if rotation is done around bottom (for positive defelction down)

        //Patch, point variables
        word topPatchName_;
        label topPatchID_;
        word bottomPatchName_;
        label bottomPatchID_;
        List<word> flapPatches_;
        List<label> flapPatchIDs_;
        List<vectorField> originalPoints_;
	    
    	//- Disallow default bitwise copy construct
        RigidFlap(const RigidFlap&);

        //- Set initial points when no patch is attached to body
        void setOriginalPoints();
	    
	public:

    //- Runtime type information
    TypeName("RigidFlap");

	//Public data

    // Constructors

        //- Construct from dict + mesh
		RigidFlap(const dictionary& dict,const polyMesh& mesh);

		virtual autoPtr<RigidMotionFunction> clone() const
        {
            return autoPtr<RigidMotionFunction>(new RigidFlap(this->dict_,this->mesh_));
        };

    // Destructor

        virtual ~RigidFlap();

    // Member Functions
    
		virtual void getPosition
		(
			const scalar& time,
			const vectorField& movingPoints,
			vectorField& coordinates
		) const;
		
        virtual void getOldPosition
        (
            const vectorField& movingPoints,
            vectorField& coordinates
        ) const;
		
        virtual label nPoints() const;
        virtual const vectorField& initialPoints() const;
		virtual const bool& moveToInitPosition() const;
};
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
