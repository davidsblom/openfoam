/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    RBFMotionSolverRigid

Description
    Radial basis function motion solver using rigid body motion

Author
    Thijs Gillebaart, TU Delft.  All rights reserved.

SourceFiles
    RBFMotionSolverRigid.C

\*---------------------------------------------------------------------------*/

#ifndef RBFMotionSolverRigid_H
#define RBFMotionSolverRigid_H

#include "motionSolver.H"
#include "polyMesh.H"
#include "meshConnection2D.H"
#include "RBFInterpolationReduced.H"
#include "RigidMotionFunction.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                      Class RBFMotionSolverRigid Declaration
\*---------------------------------------------------------------------------*/

class RBFMotionSolverRigid
:
    public motionSolver
{
    // Private data

        //- Static patch names
        wordList staticPatches_;

        //- Static patch names
        wordList movingPatches_;

        //- Coarsening ratio for static boundary
        label coarseningRatioStat_;
        
        //- Coarsening ratio for static boundary
        label coarseningRatioMov_;

        //- Control point IDs
        labelList controlIDs_;

        //- Control points on the boundary
        mutable vectorField controlPoints_;

        //- Internal point IDs
        labelList internalIDs_;

        //- Internal points
        vectorField internalPoints_;

        //- Static point IDs
        labelList staticIDs_;

        //- Static points
        vectorField staticPoints_;

        //- Moving point IDs
        labelList movingIDs_;

        //- Moving points original location
        vectorField movingPoints_;
        
        //- Motion of moving points
        vectorField motion_;
        
        //- Index of which of the moving points are used as control points
        labelList movingControlPointsIndex_;

		//- Index of where new patch starts in movingPoints_
		labelList movingPointsPatchStart_;

		//- Number of static control points
		label nStaticControlPoints_;
		
        //- RigidMotionFunction function
        autoPtr<RigidMotionFunction> rmFunction_;
		
		//- Miller pesking motion class object
		bool isMillerPeskin_;
		
		//- symmetricMotion
		//bool isSymmetricMotion_;
		//dictionary symmetricMotionDict_;

        //RBFInterpolationHans interpolation_;
        RBFInterpolationReduced interpolationRed_;

		//- Connectivity retrieval object for 2D meshes
		meshConnection2D meshConnections_;

// Private Member Functions

        //- Disallow default bitwise copy construct
        RBFMotionSolverRigid(const RBFMotionSolverRigid&);

        //- Disallow default bitwise assignment
        void operator=(const RBFMotionSolverRigid&);


        //- Make control point IDs.  Constructor helper
        void makeControlIDs();

        //- Make control point IDs for 2D
        void makeControlIDs2D();

        //- Parallel processer controlPoint gatherer
        void gatherControlPoints();

        //- Get extra discplacement field from Fourier Series
        void setDisplacementField();

        //- Get coordinates from Fourier Series approximation for time t
		vectorField getCoordinatesFromTime(scalar t) const;

        //- Returns the new point locations in the correct format to the solver
		void moveMesh(pointField& newPoints) const;

public:

    //- Runtime type information
    TypeName("RBFMotionSolverRigid");

    // Constructors

        //- Construct from polyMesh
        RBFMotionSolverRigid
        (
            const polyMesh& mesh,
            Istream& msData
        );

    // Destructor

        virtual ~RBFMotionSolverRigid();

    // Member Functions

		//- Set motion_ according to m
		void setMotion(const vectorField& m);
		
		//- Get movingPoints
		const vectorField& movingPoints() const;

        //- Return point location obtained from the current motion field
        virtual tmp<pointField> curPoints() const;

        //- Solve for motion
        virtual void solve();

        //- Update the mesh corresponding to given map
        virtual void updateMesh(const mapPolyMesh&);
        
        //-
        void setInitialMesh(pointField& points);
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
