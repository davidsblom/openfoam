/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

edit by HB 20090925: adjust for different boundary set.

\*---------------------------------------------------------------------------*/

#include "RBFMotionSolverRigid.H"
#include "addToRunTimeSelectionTable.H"
#include "processorPolyPatch.H"

using namespace std;

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(RBFMotionSolverRigid, 0);

    addToRunTimeSelectionTable
    (
        motionSolver,
        RBFMotionSolverRigid,
        dictionary
    );
}

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

void Foam::RBFMotionSolverRigid::makeControlIDs()
{
    FatalErrorIn("void RBFMotionSolverRigid::makeControlIDs()")
    << "Function not present for 3D. Implementation needed!"
    << abort(FatalError);
}

void Foam::RBFMotionSolverRigid::makeControlIDs2D()
{
	Info << endl << "RBFMotionSolverRigid::makeControlIDs2D" << endl;
   	
   	const labelList& conPointIDs(meshConnections_.getMeshIDList());
   	const labelList& conPointIDLoc(meshConnections_.getLocationList());
	const label n2DPoints = conPointIDs.size()/2;
	labelList markedPoints(n2DPoints,0); //to determine the internal points

	const pointField& points = mesh().points();

    // ======================================================================================//
    // ============================= Static Points ==========================================//
    // ======================================================================================//

	//If parallel run make sure that only the owner of processor patches get the static boundary points
	if(Pstream::parRun())
	{
		const polyBoundaryMesh& bMesh(mesh().boundaryMesh());
		const labelList& procPatches(mesh().globalData().processorPatches());
		forAll(procPatches,i)//For all processor patches
		{
			const processorPolyPatch& procPatch = refCast<const processorPolyPatch>(bMesh[procPatches[i]]);
			const labelList& procPatchPoints = procPatch.meshPoints();

			if(procPatch.neighbour())//If not the owner of the patch
			{
				forAll(procPatchPoints,k)//Go over all points and mark them with -3 if on left side
				{
					if(conPointIDLoc[procPatchPoints[k]]<n2DPoints)
					{
						markedPoints[conPointIDLoc[procPatchPoints[k]]]=-3;
					}
				}
			}
		}
	}

    int nMarkedPoints = 0;
	int pickedPoint = 0;
	int nNeighbourPoints = 0; //For parallel processing

    forAll (staticPatches_, patchI)
    {
        label patchIndex = mesh().boundaryMesh().findPatchID(staticPatches_[patchI]);

        if (patchIndex < 0)
        {
            FatalErrorIn("void RBFMotionSolver::makeControlPoints()")
                << "Patch " << staticPatches_[patchI] << " not found.  "
                << "valid patch names: " << mesh().boundaryMesh().names()
                << abort(FatalError);
        }

        const labelList& mp = mesh().boundaryMesh()[patchIndex].meshPoints();
		staticIDs_.setSize(staticIDs_.size()+mp.size());
		staticPoints_.setSize(staticIDs_.size());
		controlIDs_.setSize(controlIDs_.size()+mp.size());
		controlPoints_.setSize(controlIDs_.size());

        forAll (mp, i)
        {
        	// Only points in left plane and not tagged as static point or corner point already or as neighbour processor point
            if(conPointIDLoc[mp[i]]<n2DPoints && markedPoints[conPointIDLoc[mp[i]]]!=-1 && markedPoints[conPointIDLoc[mp[i]]]!=-2)
        	{
        		staticIDs_[nMarkedPoints]=mp[i];
        		staticPoints_[nMarkedPoints]=points[mp[i]];

	            //Static control point selection
	            if(pickedPoint*coarseningRatioStat_==nMarkedPoints-nNeighbourPoints)
	            {
	            	if(markedPoints[conPointIDLoc[mp[i]]]==-3)//If is a neighbour CPU point dont use it
	            	{
	            		nNeighbourPoints++;
	            	}
	            	else
	            	{
						// Pick point as control point
						controlIDs_[pickedPoint] = mp[i];
						controlPoints_[pickedPoint] = points[mp[i]];

						// Mark the point as picked
						markedPoints[conPointIDLoc[mp[i]]] = -2;

						pickedPoint++;
					}
	            }
	            else
	            {
   	   	            markedPoints[conPointIDLoc[mp[i]]]=-1;
	            }
   	            nMarkedPoints++;
        	}
        }
    }

	staticIDs_.setSize(nMarkedPoints);
	staticPoints_.setSize(nMarkedPoints);
	controlIDs_.setSize(pickedPoint);
	controlPoints_.setSize(pickedPoint);

    Info << "Total points on static boundaries: " << staticIDs_.size() << endl;
    Info << "Total picked static control points: " << controlIDs_.size() << endl;
    
    // ======================================================================================//
    // ============================= Moving Points ==========================================//
    // ======================================================================================//
    // Re-use counters
    nStaticControlPoints_ = controlIDs_.size();
    nMarkedPoints = 0;
	pickedPoint = 0;
	nNeighbourPoints = 0; //For parallel processing

    forAll (movingPatches_, patchI)
    {
        label patchIndex = mesh().boundaryMesh().findPatchID(movingPatches_[patchI]);

        if (patchIndex < 0)
        {
            FatalErrorIn("void RBFMotionSolver::makeControlPoints()")
                << "Patch " << staticPatches_[patchI] << " not found.  "
                << "valid patch names: " << mesh().boundaryMesh().names()
                << abort(FatalError);
        }

        const labelList& patchPointIDs = mesh().boundaryMesh()[patchIndex].meshPoints();
		movingIDs_.setSize(movingIDs_.size()+patchPointIDs.size());
		movingPoints_.setSize(movingIDs_.size());
		movingControlPointsIndex_.setSize(movingIDs_.size());
		controlIDs_.setSize(controlIDs_.size()+patchPointIDs.size());
		controlPoints_.setSize(controlIDs_.size());
		movingPointsPatchStart_[0]=0;

        forAll (patchPointIDs, i)
        {
        	// Only points in left plane and not tagged as static point or corner point already or as neighbour processor point
            if(conPointIDLoc[patchPointIDs[i]]<n2DPoints && markedPoints[conPointIDLoc[patchPointIDs[i]]]!=-1 && markedPoints[conPointIDLoc[patchPointIDs[i]]]!=-2)
        	{
        		movingIDs_[nMarkedPoints]=patchPointIDs[i];
        		movingPoints_[nMarkedPoints]=points[patchPointIDs[i]];

	            //Moving control point selection
	            if(pickedPoint*coarseningRatioMov_==nMarkedPoints-nNeighbourPoints)
	            {
	            	if(markedPoints[conPointIDLoc[patchPointIDs[i]]]==-3)//If is a neighbour CPU point -> dont use it
	            	{
	            		nNeighbourPoints++;
	            	}
	            	else
	            	{
						// Pick point as control point
						controlIDs_[nStaticControlPoints_ + pickedPoint] = patchPointIDs[i];
						controlPoints_[nStaticControlPoints_ + pickedPoint] = points[patchPointIDs[i]];

						// Mark the point as picked
						markedPoints[conPointIDLoc[patchPointIDs[i]]] = -2;
						movingControlPointsIndex_[pickedPoint] = nMarkedPoints;
						
						pickedPoint++;
					}
	            }
	            else
	            {
   	   	            markedPoints[conPointIDLoc[patchPointIDs[i]]]=-1;
	            }
   	            nMarkedPoints++;
        	}
        }
        if(patchI!=movingPatches_.size()-1)
        {
        	movingPointsPatchStart_[patchI+1] = nMarkedPoints;
        }
    }
	Info << "movingPointsPatchStart_ = " << movingPointsPatchStart_ << endl;

	movingIDs_.setSize(nMarkedPoints);
	movingPoints_.setSize(nMarkedPoints);
	movingControlPointsIndex_.setSize(pickedPoint);
	controlIDs_.setSize(nStaticControlPoints_ + pickedPoint);
	controlPoints_.setSize(nStaticControlPoints_ + pickedPoint);

    Info << "Total points on moving boundaries: " << movingPoints_.size() << endl;
    Info << "Total picked moving control points: " << pickedPoint << endl;
    Info << "Total picked control points: " << controlIDs_.size() << endl;
    
    // ======================================================================================//
    // ============================= Internal Points ========================================//
    // ======================================================================================//

    internalIDs_.setSize(n2DPoints);
    internalPoints_.setSize(n2DPoints);

    // Re-use counter
    nMarkedPoints = 0;

    forAll (markedPoints, i)
    {
        if (markedPoints[i] == 0 || markedPoints[i]==-3)//Maybe displacing patch points on processor slave patches
        {
            internalIDs_[nMarkedPoints] = conPointIDs[i];
            internalPoints_[nMarkedPoints] = points[conPointIDs[i]];
            nMarkedPoints++;
        }
    }
	internalIDs_.setSize(nMarkedPoints);
	internalPoints_.setSize(nMarkedPoints);

    Info << "Number of internal points: " << nMarkedPoints << endl;
	
	if(Pstream::parRun())
	{
    	gatherControlPoints();
    }
}

void Foam::RBFMotionSolverRigid::gatherControlPoints()
{
	//Gather all control points
	List<vectorField> controlPointList(Pstream::nProcs());
	if(Pstream::master())
	{
		controlPointList[0]=controlPoints_;
		for(label i=Pstream::firstSlave();i<=Pstream::lastSlave();i++)
		{
			IPstream pointStream(Pstream::blocking,i);
			pointStream >> controlPointList[i];
		}
	}
	else
	{
		OPstream oStream(Pstream::blocking,Pstream::masterNo());
		oStream << controlPoints_;
	}
	
	//Determine number of static control points per proc
	List<scalar> globalNStaticControlPoints(Pstream::nProcs());
	if(Pstream::master())
	{
		globalNStaticControlPoints[0]=nStaticControlPoints_;
		for(label i=Pstream::firstSlave();i<=Pstream::lastSlave();i++)
		{
			IPstream iStream(Pstream::blocking,i);
			iStream >> globalNStaticControlPoints[i];
		}
	}
	else
	{
		OPstream oStream(Pstream::blocking,Pstream::masterNo());
		oStream << nStaticControlPoints_;
	}

	//Reorganize into a single list with all control points and send back to slaves
	if(Pstream::master())
	{
		//First determine total number of control points and total number of static control points
		label nTotalStaticControlPoints = sum(globalNStaticControlPoints);
		label nTotalControlPoints(0);
		for(label i=0;i<controlPointList.size();i++)
		{
			nTotalControlPoints = nTotalControlPoints + controlPointList[i].size();
		}
		Info << "ParRUN: Total number of control points = " << nTotalControlPoints << endl;
		Info << "ParRUN: Total number of static control points = " << nTotalStaticControlPoints << endl;
		
		vectorField globalControlPoints(nTotalControlPoints);
		label counterStatic(0);
		label counterMoving(nTotalStaticControlPoints);
		for(label i=0;i<controlPointList.size();i++)
		{
			for(label j=0;j<globalNStaticControlPoints[i];j++)//Copy static points
			{
				globalControlPoints[counterStatic+j]=controlPointList[i][j];
			}
			counterStatic=counterStatic+globalNStaticControlPoints[i];
			
			label nMovingControlPoints = controlPointList[i].size() - globalNStaticControlPoints[i];
			for(label j=0;j<nMovingControlPoints;j++)//Copy moving points
			{
				globalControlPoints[counterMoving+j] = controlPointList[i][globalNStaticControlPoints[i]+j];
			}
			counterMoving = counterMoving + nMovingControlPoints;
		}
		controlPoints_ = globalControlPoints;
		nStaticControlPoints_ = nTotalStaticControlPoints;//Update number of static control points on master

		for(label i=Pstream::firstSlave();i<=Pstream::lastSlave();i++)
		{
			OPstream contPointStream(Pstream::blocking,i);
			contPointStream << controlPoints_;
		}
	}
	else
	{
		IPstream contPointStream(Pstream::blocking,0);
		contPointStream >> controlPoints_;
	}

	//Update the total number of static points on all slaves
	if(Pstream::master())
	{
		for(label i=Pstream::firstSlave();i<=Pstream::lastSlave();i++)
		{
			OPstream nStaticPointsStream(Pstream::blocking,i);
			nStaticPointsStream << nStaticControlPoints_;
		}
	}
	else
	{
		IPstream nStaticPointsStream(Pstream::blocking,Pstream::masterNo());
		nStaticPointsStream >> nStaticControlPoints_;
	}
	
}

void Foam::RBFMotionSolverRigid::setDisplacementField()
{
	Info << "RBFMotionSolverRigid::setDisplacementField() -> Obtain motion from rigid body motion" << endl;
	const scalar& time = mesh().time().value();

	vectorField prevPoints(movingPoints_.size(),vector::zero);
	rmFunction_->getPosition(time,movingPoints_,motion_);
	rmFunction_->getOldPosition(movingPoints_,prevPoints);
	
	//vectorField curPoints = getCoordinatesFromTime(mesh().time().value());
	//vectorField prevPoints = getCoordinatesFromTime(mesh().time().value()-mesh().time().deltaT().value());
	motion_ = motion_ - prevPoints;
}

Foam::vectorField Foam::RBFMotionSolverRigid::getCoordinatesFromTime(scalar time) const
{
	vectorField coordinates(movingPoints_.size(),vector::zero);
	
	rmFunction_->getPosition(time,movingPoints_,coordinates);

	return coordinates;
}

void Foam::RBFMotionSolverRigid::moveMesh(pointField& newPoints) const
{
	vectorField movingControlPointsMotion(movingControlPointsIndex_.size());
	forAll(movingControlPointsMotion,i)
	{	
		movingControlPointsMotion[i] = motion_[movingControlPointsIndex_[i]];
	}

    if(Pstream::parRun() && !isMillerPeskin_)
    {
		//Gather all movingControlPointsMotion and group them
		List<vectorField> listMovingControlPoints(Pstream::nProcs());
		listMovingControlPoints[0]=movingControlPointsMotion;
		if(Pstream::master())
		{
			for(label i=Pstream::firstSlave();i<=Pstream::lastSlave();i++)
			{
				IPstream iStream(Pstream::blocking,i);
				iStream >> listMovingControlPoints[i];
			}
			
			movingControlPointsMotion = vectorField(controlPoints_.size() - nStaticControlPoints_);
			label counter(0);
			forAll(listMovingControlPoints,i)
			{
				forAll(listMovingControlPoints[i],j)
				{
					movingControlPointsMotion[j+counter] = listMovingControlPoints[i][j];
				}
				counter = counter + listMovingControlPoints[i].size();
			}
		}
		else
		{
			OPstream oStream(Pstream::blocking,Pstream::masterNo());
			oStream << movingControlPointsMotion;
		}
		
		//Send movingControlPointsMotion back to slaves
		if(Pstream::master())
		{
			for(label i=Pstream::firstSlave();i<=Pstream::lastSlave();i++)
			{
				OPstream oStream(Pstream::blocking,i);
				oStream << movingControlPointsMotion;
			}
		}
		else
		{
			IPstream iStream(Pstream::blocking,Pstream::masterNo());
			iStream >> movingControlPointsMotion;
		}
    }
    
    // Call interpolation
	vectorField interpolatedMotion = interpolationRed_.interpolateRed(movingControlPointsMotion);

    forAll (internalIDs_, i)
    {
		newPoints[internalIDs_[i]] = interpolatedMotion[i];
    }
    forAll(movingIDs_,i)
    {
    	newPoints[movingIDs_[i]] = motion_[i];
    }

	if(word(subDict("interpolation").lookup("Dim"))=="TwoD")
    {
    	const labelList& conPointIDs(meshConnections_.getMeshIDList());
   		const labelList& conPointIDLoc(meshConnections_.getLocationList());
		const label n2DPoints = conPointIDs.size()/2;

   		forAll(internalIDs_,i)
   		{
   			label relatedID = conPointIDs[conPointIDLoc[internalIDs_[i]]+n2DPoints];
   			newPoints[relatedID]=newPoints[internalIDs_[i]];
   		}
   		forAll(movingIDs_,i)
   		{
			label relatedID = conPointIDs[conPointIDLoc[movingIDs_[i]]+n2DPoints];
   			newPoints[relatedID]=newPoints[movingIDs_[i]];
   		}
    }
    
    //Ensures that the displacement on the processor boundary is the same
	if(Pstream::parRun())
	{
		const polyBoundaryMesh& bMesh(mesh().boundaryMesh());
		const labelList& procPatches(mesh().globalData().processorPatches());
		forAll(procPatches,i)//For all processor patches
		{
			const processorPolyPatch& procPatch = refCast<const processorPolyPatch>(bMesh[procPatches[i]]);
			const labelList& procPatchPointIDs = procPatch.meshPoints();

			if(procPatch.neighbour())//If not the owner of the patch receive boundary point location
			{
				pointField procPatchOwnerPoints(procPatchPointIDs.size());
				IPstream pointStream(Pstream::blocking,procPatch.neighbProcNo());
				pointStream >> procPatchOwnerPoints;

				forAll(procPatchOwnerPoints,i)
				{
					newPoints[procPatchPointIDs[i]]=procPatchOwnerPoints[i];
				}
			}
			else //If the owner send the boundary points
			{
				//Create pointList
				pointField procPatchPoints(procPatchPointIDs.size());
				forAll(procPatchPoints,i)
				{
					procPatchPoints[i]=newPoints[procPatchPointIDs[i]];
				}
				OPstream pointStream(Pstream::blocking,procPatch.neighbProcNo());
				pointStream << procPatchPoints;
			}
		}
	}
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::RBFMotionSolverRigid::RBFMotionSolverRigid
(
    const polyMesh& mesh,
    Istream&
)
:
motionSolver(mesh),
staticPatches_(lookup("staticPatches")),
movingPatches_(lookup("movingPatches")),
coarseningRatioStat_(readLabel(lookup("coarseningRatioStat"))),
coarseningRatioMov_(readLabel(lookup("coarseningRatioMov"))),
controlIDs_(0),
controlPoints_(0),
internalIDs_(0),
internalPoints_(0),
staticIDs_(0),
staticPoints_(0),
movingIDs_(0),
movingPoints_(0),
motion_(0),
movingControlPointsIndex_(0),
movingPointsPatchStart_(movingPatches_.size()),
nStaticControlPoints_(0),
rmFunction_(RigidMotionFunction::New(word(lookup("RigidMotionFunction")), this,mesh)),
isMillerPeskin_(rmFunction_->type()=="MillerPeskin"),
//isSymmetricMotion_(subDict("rigidBodyMotion").isDict("symmetricMotion")),
//symmetricMotionDict_(),
interpolationRed_
(
    mesh,
    subDict("interpolation"),
    controlPoints_,
    internalPoints_
)
{
   	if(word(subDict("interpolation").lookup("Dim"))=="TwoD")
   	{
   		Info << "Two-dimensional meshmotion detected" << endl;
   		meshConnections_ = meshConnection2D(mesh);
		makeControlIDs2D();//THIJS
    }
    else
    {
    	makeControlIDs();
    }
	motion_.setSize(movingIDs_.size(),vector::zero);
	
	/*if(isSymmetricMotion_)
	{
		symmetricMotionDict_ = subDict("rigidBodyMotion").subDict("symmetricMotion");
		//First perform some checks
		if(movingPatches_.size() != 2)
		{
			    FatalErrorIn("void RBFMotionSolverRigid::RBFMotionSolverRigid()")
			    << "Number of moving pathces is not equal to 2. This is needed for symmetric motion"
			    << abort(FatalError);
		}
		//Check which is the first moving patch: original OR reflected
		symmetricMotionDict_.add("originalFirst",bool(movingPatches_[0]==word(symmetricMotionDict_.lookup("originalPatch"))));
		if(bool(readLabel(symmetricMotionDict_.lookup("originalFirst"))))
		{
			if(movingPatches_[1] != word(symmetricMotionDict_.lookup("symmetricPatch")))
			{
				FatalErrorIn("void RBFMotionSolverRigid::RBFMotionSolverRigid()")
			    << "Symmetric patch is not equal to second moving patch"
			    << abort(FatalError);
			}
		}
		else
		{
			if(movingPatches_[1] != word(symmetricMotionDict_.lookup("originalPatch")) || movingPatches_[0] != word(symmetricMotionDict_.lookup("symmetricPatch")))
			{
				FatalErrorIn("void RBFMotionSolverRigid::RBFMotionSolverRigid()")
			    << "Moving patches not equal to symmetric and original patch"
			    << abort(FatalError);
			}
		}
	
		//Add all needed variables to dict, such that recalculation is not needed every time step
		vector symplaneNormal = symmetricMotionDict_.lookup("symplaneNormal");
		symplaneNormal=symplaneNormal/mag(symplaneNormal);
		vector transDirRefl = translationDirection_ - 2*(translationDirection_ & symplaneNormal)*symplaneNormal;
		
		vector symplaneLoc = symmetricMotionDict_.lookup("symplanePos");
		vector rotationVector = rotationOrigin_ - symplaneLoc;
		vector rotOrigRefl = rotationVector - 2*(rotationVector & symplaneNormal)*symplaneNormal + symplaneLoc;
		
		symmetricMotionDict_.set("symplaneNormal",symplaneNormal);
		symmetricMotionDict_.add("translationDirectionReflected",transDirRefl);
		symmetricMotionDict_.add("rotationOriginReflected",rotOrigRefl);
	}*/
	
	if(movingPatches_.size()<1)
	{
		Info << "No moving patches are given! Assuming zeroThickness wing which will be constructed based on the RigidMotionFunction. " << endl;
		//Reset movingControlPointsIndex_ and controlPoints_ (only works for MillerPeskin motion)
		label nPoints = rmFunction_->nPoints();
		label nControlPoints(controlPoints_.size());
		controlPoints_.setSize(nControlPoints+2*nPoints);
		movingControlPointsIndex_.setSize(2*nPoints);
		vectorField extraPoints(2*nPoints,vector::zero);
		if(rmFunction_->moveToInitPosition())
		{
			extraPoints = rmFunction_->initialPoints();
		}
		else
		{
			const scalar& startTime = mesh.time().startTime().value();
			rmFunction_->getPosition(startTime,movingPoints_,extraPoints);
		}
		
		forAll(extraPoints,i)
		{
			controlPoints_[nControlPoints+i] = extraPoints[i];
			movingControlPointsIndex_[i]=i;
		}
		Info << "controlPoints_ = " << controlPoints_ << endl;
	}

	//Construct matrix for RBF interpolation
	label nPoints = controlPoints_.size()-nStaticControlPoints_;
	labelList movingControlPointsInd(nPoints);
	for(label i=nPoints;i>0;i--)
	{
		//Last controlPoints are the moving control points
		movingControlPointsInd[nPoints-i]=controlPoints_.size()-i;
	}
    interpolationRed_.createReducedEvaluationMatrix(movingControlPointsInd);
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::RBFMotionSolverRigid::~RBFMotionSolverRigid()
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void Foam::RBFMotionSolverRigid::setMotion(const vectorField& m)
{
    if (m.size() != movingIDs_.size())
    {
        FatalErrorIn
        (
            "void RBFMotionSolverRigid::setMotion(const vectorField& m)"
        )   << "Incorrect size of motion points: m = " << m.size()
            << " movingIDs = " << movingIDs_.size()
            << abort(FatalError);
    }

    motion_ = vector::zero;

    forAll (m, i)
    {
        motion_[i] = m[i];
    }
}

const Foam::vectorField& Foam::RBFMotionSolverRigid::movingPoints() const
{
    return movingPoints_;
}

Foam::tmp<Foam::pointField> Foam::RBFMotionSolverRigid::curPoints() const
{
	Info<<"Foam::RBFMotionSolverRigid::curPoints()" <<endl;

    // Prepare new points: same as old point
    tmp<pointField> tnewPoints
    (
        new vectorField(mesh().nPoints(), vector::zero)
    );

    pointField& newPoints = tnewPoints();

	//Info << "motion_ = " << motion_ << endl;

	// Write the new point discplacement to newPoints
	moveMesh(newPoints);

    // 4. Add old point positions
    newPoints += mesh().points();

    return tnewPoints;
}

void Foam::RBFMotionSolverRigid::solve()
{
	Info << "Foam::RBFMovtionSolver::solve()" << endl; //added by Thijs

    // Write motion to motion_
	setDisplacementField();
}


void Foam::RBFMotionSolverRigid::updateMesh(const mapPolyMesh&)
{
    // Recalculate control point IDs
    makeControlIDs();
}

void Foam::RBFMotionSolverRigid::setInitialMesh(pointField& points)
{
    Info << "Foam::RBFMotionSolverRigid::setInitialMesh()" <<endl;

    // Prepare new points: same as old point
    tmp<pointField> tnewPoints
    (
        new vectorField(mesh().nPoints(), vector::zero)
    );

    pointField& newPoints = tnewPoints();

	vectorField extraMotion(movingPoints_.size(),vector::zero);
	const scalar& startTime = mesh().time().startTime().value();
	rmFunction_->getPosition(startTime,movingPoints_,motion_);//extraMotion is now equal to first location
	if(movingPatches_.size()<1)
	{
		motion_ = motion_ - rmFunction_->initialPoints();
	}
	else
	{
		motion_ = motion_ - movingPoints_;
	}
	// Write the new point discplacement to newPoints
	moveMesh(newPoints);

    // 4. Add old point positions
    newPoints += mesh().points();

	points = newPoints;
}

// ************************************************************************* //
