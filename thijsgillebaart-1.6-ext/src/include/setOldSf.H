forAll(oldSf.internalField(),iFace)
{
	oldSf.internalField()[iFace] = mesh.Sf().internalField()[iFace];
}
forAll(oldSf.boundaryField(),iBoundary)
{
	forAll(oldSf.boundaryField()[iBoundary],iFace)
	{
		oldSf.boundaryField()[iBoundary][iFace] = mesh.Sf().boundaryField()[iBoundary][iFace];
	}
}