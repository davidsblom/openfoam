# OpenFOAM

OpenFOAM is a free, open source CFD software package developed by OpenCFD Ltd at ESI Group and distributed 
by the OpenFOAM Foundation . It has a large user base across most areas of engineering and science, from 
both commercial and academic organisations. OpenFOAM has an extensive range of features to solve anything 
from complex fluid flows involving chemical reactions, turbulence and heat transfer, to solid dynamics and 
electromagnetics.

This repository includes the sources and build scripts for OpenFOAM 2.2.x and OpenFOAM 1.6 extend.
