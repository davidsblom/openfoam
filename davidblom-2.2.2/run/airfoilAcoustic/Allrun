#!/bin/sh

# Determine OpenFOAM version
OF_MAJOR=`echo $WM_PROJECT_VERSION | cut -d'.' -f1`
OF_MINOR=`echo $WM_PROJECT_VERSION | cut -d'.' -f2`

# Set variable to distinguish between OF 2.2 and older versions
if [ $OF_MAJOR -le 2 -a $OF_MINOR -le 1 ]; then
  OLD=1
else
  OLD=0
fi

# Copy the mesh from the unsteady state case airfoil and map the results to a
# transient case, then solve transient.
# The airfoil case does not solve the acoustic field. This project does solve 
# the acoustic field.
cd airfoil_acousticFoam
cp -r ../../airfoil/airfoil_pimpleFoam/constant/polyMesh/* constant/polyMesh/
rm -f 0/*
cp 0.org/* 0/
mapFields ../../airfoil/airfoil_pimpleFoam -sourceTime latestTime -consistent > output/1-mapFields.log 2>&1
mv 0/gammaField.unmapped 0/gammaField
decomposePar > output/2-decomposePar.log 2>&1
foamJob -parallel -screen acousticFoam > output/3-acousticFoam.log 2>&1

reconstructPar > output/4-reconstructPar.log 2>&1

if [ $OLD -eq "1" ]; then
  ./liftDrag-2.1.plot
else
  ./liftDrag.plot
fi

foamToVTK > output/5-foamToVTK.log 2>&1

touch airfoil_pimpleFoam.foam
