#!/bin/bash

####################################################################################
#
# Script to download and build OpenFOAM including third party packages
#
# Note: ParaView is NOT build. Binaries of ParaView can be downloaded from
# http://www.paraview.org/. The simulation results of OpenFOAM can be converted to
# VTK with the foamToVTK utility included in OpenFOAM.
#
# Dependencies: gcc (version 4.7.2 or higher)
#
# Reference: http://www.openfoam.org/download/source.php
#
# Author: David Blom (d.s.blom@tudelft.nl)
#
####################################################################################

OPENFOAM_VERSION=dev

set -e

cd ..

# Remove temporary directories and files

find . -maxdepth 1 -name '*~'
rm -rf ThirdParty-${OPENFOAM_VERSION}

# Download OpenFOAM source and third party packages

wget -O ThirdParty-${OPENFOAM_VERSION}.tgz http://downloads.sourceforge.net/foam/foam/2.4.0/ThirdParty-2.4.0.tgz

# Unpack packages

mv ThirdParty-${OPENFOAM_VERSION}.tgz install/
cd install
tar xzf ThirdParty-${OPENFOAM_VERSION}.tgz
rm -rf ThirdParty-${OPENFOAM_VERSION}.tgz
mv ThirdParty-2.4.0 ../ThirdParty-${OPENFOAM_VERSION}
cd ..

# Setting environment variables for alternative installation location

export FOAM_INST_DIR=`pwd`
foamDotFile=$FOAM_INST_DIR/OpenFOAM-${OPENFOAM_VERSION}/etc/bashrc

export WM_CC="mpicc"
export WM_CXX="mpicxx"

source $foamDotFile
export WM_THIRD_PARTY_DIR=$FOAM_INST_DIR/ThirdParty-${OPENFOAM_VERSION}

echo $WM_THIRD_PARTY_DIR
echo $WM_PROJECT_DIR

# Build configuration by using all available cores

export WM_NCOMPPROCS=4

cd $WM_PROJECT_DIR

# Building the sources

./Allwmake > make_openfoam.log 2> make_openfoam.error.log
./Allwmake
