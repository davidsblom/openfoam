#!/bin/bash

set -e

rm -rf ~/foam

VERSION=3.2

cd ..

export PARAVIEW_SYSTEM=1
export CUDA_IGNORE=1
export SWAK4FOAM_SYSTEM=1

# System installed MPI
export WM_MPLIB=INTELMPI

# System installed hwloc
export HWLOC_SYSTEM=1
export HWLOC_DIR=$HWLOC_BASE
export HWLOC_BIN_DIR=$HWLOC_DIR/bin

# System installed CMake
export CMAKE_SYSTEM=1
export CMAKE_DIR=$CMAKE_BASE
export CMAKE_BIN_DIR=$CMAKE_DIR/bin

# System install bison
export BISON_SYSTEM=1
export BISON_DIR=$BISON_BASE
export BISON_BIN_DIR=$BISON_DIR/bin

export WM_THIRD_PARTY_USE_SCOTCH_600=1

export FOAM_INST_DIR=`pwd`
WM_THIRD_PARTY_DIR=$FOAM_INST_DIR/foam-extend-${VERSION}/ThirdParty

# Copy patch file to foam-extend/src/settings
cp install/settings.patch $FOAM_INST_DIR/foam-extend-${VERSION}/etc/

# Clean previous compilation files

cd $FOAM_INST_DIR/foam-extend-${VERSION}

sed -i s/"CC          = g++ -m64"/"CC          = mpicxx -m64"/g wmake/rules/linux64Gcc/c++
sed -i s/"c++OPT      = -O3"/"c++OPT      = -O3 -DNDEBUG"/g wmake/rules/linux64Gcc/c++Opt

source etc/bashrc

# Use system bison
sed -i s/"( rpm_make -p bison"/"#( rpm_make -p bison"/g ThirdParty/AllMake.stage1
# Use system openmpi
sed -i s/"( rpm_make -p openmpi-1.6"/"#( rpm_make -p openmpi-1.6"/g ThirdParty/AllMake.stage2
# MPI flags
cp $FOAM_INST_DIR/OpenFOAM-2.4.x/wmake/rules/linux64Gcc/mplibINTELMPI $FOAM_INST_DIR/foam-extend-${VERSION}/wmake/rules/linux64Gcc/mplibINTELMPI

# Patch settings.sh
cd etc
patch < settings.patch
cd ..

# Compile OpenFOAM extend

# This next command will take a while... somewhere between 30 minutes to 3-6 hours.
./Allwmake.firstInstall > >(tee make_openfoam.log) 2> >(tee make_openfoam_error.log >&2)

# Run it a second time for getting a summary of the installation
./Allwmake > >(tee make_openfoam.log) 2> >(tee make_openfoam_error.log >&2)

source etc/bashrc

# Check if icoFoam is working
icoFoam -help

# Compile third party packages

cd ${FOAM_INST_DIR}/davidblom-3.1-ext/src/thirdParty

./compile_supermuc

cd ${FOAM_INST_DIR}/davidblom-3.1-ext

pwdstring=`pwd`
prefLocation=$WM_PROJECT_DIR/etc/prefs.sh

echo "export FOAM_USER_SRC=$pwdstring/src" >> $prefLocation
echo "export PARAVIEW_SYSTEM=1" >> $prefLocation
echo "export CUDA_IGNORE=1" >> $prefLocation
echo "export SWAK4FOAM_SYSTEM=1" >> $prefLocation
echo "export WM_MPLIB=INTELMPI" >> $prefLocation
echo "export HWLOC_SYSTEM=1" >> $prefLocation
echo "export HWLOC_DIR=$HWLOC_BASE" >> $prefLocation
echo "export HWLOC_BIN_DIR=$HWLOC_DIR/bin" >> $prefLocation
echo "export CMAKE_SYSTEM=1" >> $prefLocation
echo "export CMAKE_DIR=$CMAKE_BASE" >> $prefLocation
echo "export CMAKE_BIN_DIR=$CMAKE_DIR/bin" >> $prefLocation
echo "export BISON_SYSTEM=1" >> $prefLocation
echo "export BISON_DIR=$BISON_BASE" >> $prefLocation
echo "export BISON_BIN_DIR=$BISON_DIR/bin" >> $prefLocation
echo "export FOAM_INST_DIR=$FOAM_INST_DIR" >> $prefLocation
echo "WM_THIRD_PARTY_DIR=$FOAM_INST_DIR/foam-extend-${VERSION}/ThirdParty" >> $prefLocation

source $FOAM_SRC/../etc/bashrc

./Allwmake
