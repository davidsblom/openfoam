#!/bin/bash

set -e

VERSION=3.1

cd ..

find . -maxdepth 1 -name '*~' -exec rm -rf {} \;

# Setting environment variables for alternative installation location

export FOAM_INST_DIR=`pwd`
WM_THIRD_PARTY_DIR=$FOAM_INST_DIR/foam-extend-${VERSION}/ThirdParty

rm -rf ~/foam

# Use the system installed libraries
export PARAVIEW_SYSTEM=1
export CUDA_IGNORE=1
export SWAK4FOAM_SYSTEM=1
export WM_MPLIB=SYSTEMOPENMPI
export OPENMPI_DIR=/cm/shared/apps/openmpi/gcc/64/1.6.5
export OPENMPI_BIN_DIR=/cm/shared/apps/openmpi/gcc/64/1.6.5/bin
export WM_THIRD_PARTY_USE_M4_146=1
export WM_THIRD_PARTY_USE_FLEX_2535=1
export WM_THIRD_PARTY_USE_CMAKE_322=1
export WM_THIRD_PARTY_USE_SCOTCH_600=1

# Clean previous compilation files

cd $FOAM_INST_DIR/foam-extend-${VERSION}
source etc/bashrc

ln -fs /usr/bin/make bin/gmake

cd ThirdParty
./AllClean
rm -f *.log
rm -rf rpmBuild/BUILD/* rpmBuild/RPMS/* rpmBuild/rpmDB/* packages/* rpmBuild/tmp/*
cd ..

./Allclean

# Compile OpenFOAM extend

# This next command will take a while... somewhere between 30 minutes to 3-6 hours.
./Allwmake.firstInstall > >(tee make_openfoam_firstInstall.log) 2> >(tee make_openfoam_firstInstall_error.log >&2)

# Run it a second time for getting a summary of the installation
./Allwmake > >(tee make_openfoam.log) 2> >(tee make_openfoam_error.log >&2)

source etc/bashrc

# Compile all the personal solvers

cd $FOAM_INST_DIR/davidblom-${VERSION}-ext

./Allwmake.firstInstall
