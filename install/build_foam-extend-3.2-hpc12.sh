#!/bin/bash

set -e

VERSION=3.2

cd ..

find . -maxdepth 1 -name '*~' -exec rm -rf {} \;

# Setting environment variables for alternative installation location

export FOAM_INST_DIR=`pwd`
WM_THIRD_PARTY_DIR=$FOAM_INST_DIR/foam-extend-${VERSION}/ThirdParty

rm -rf ~/foam

# Use the system installed libraries
export PARAVIEW_SYSTEM=1
export CUDA_IGNORE=1
export SWAK4FOAM_SYSTEM=1
export WM_MPLIB=SYSTEMOPENMPI
export OPENMPI_DIR=/opt/ud/openmpi-1.8.8
export OPENMPI_BIN_DIR=$OPENMPI_DIR/bin

# Clean previous compilation files

cd $FOAM_INST_DIR/foam-extend-${VERSION}

sed -i s/"CC          = g++ -m64"/"CC          = mpicxx -m64"/g wmake/rules/linux64Gcc/c++

source etc/bashrc

ln -fs /usr/bin/make bin/gmake

# Compile OpenFOAM extend

# This next command will take a while... somewhere between 30 minutes to 3-6 hours.
./Allwmake.firstInstall > >(tee make_openfoam.log) 2> >(tee make_openfoam_error.log >&2)

# Run it a second time for getting a summary of the installation
./Allwmake > >(tee make_openfoam.log) 2> >(tee make_openfoam_error.log >&2)

source etc/bashrc

# Compile all the personal solvers

cd $FOAM_INST_DIR/davidblom-3.1-ext

./Allwmake.firstInstall
