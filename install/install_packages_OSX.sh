#!/bin/bash

set -x
sudo port -fp uninstall installed
sudo rm -rf \
        /opt/local \
        /Applications/DarwinPorts \
        /Applications/MacPorts \
        /Library/LaunchDaemons/org.macports.* \
        /Library/Receipts/DarwinPorts*.pkg \
        /Library/Receipts/MacPorts*.pkg \
        /Library/StartupItems/DarwinPortsStartup \
        /Library/Tcl/darwinports1.0 \
        /Library/Tcl/macports1.0 \
        ~/.macports
rm -f MacPorts-2.3.4-10.11-ElCapitan.pkg
curl https://distfiles.macports.org/MacPorts/MacPorts-2.3.4-10.11-ElCapitan.pkg -o MacPorts-2.3.4-10.11-ElCapitan.pkg
sudo installer -pkg MacPorts-2.3.4-10.11-ElCapitan.pkg -target /
set -e
sudo port selfupdate
sudo port install gcc5
sudo port select --set gcc mp-gcc5
sudo port install openmpi-default +gcc5
sudo port select --set mpi openmpi-mp-fortran
set -e
sudo port install cgal
sudo port select --set python python27
sudo port install ccache
sudo port install flex
sudo port install cmake gsed rpm bison wget scons scotch metis parmetis scotch
sudo port install wget

export OMPI_CC='gcc'
export OMPI_CXX='g++'
