#!/bin/bash

set -e

sudo apt-get install -y aptitude

# Update currently installed packages
sudo apt-get clean

sudo add-apt-repository -y ppa:webupd8team/atom
sudo aptitude -y update
sudo aptitude -y upgrade

sudo aptitude -y install python-sympy python-lxml atom uncrustify libiberty-dev lcov liblapack-dev libscotch-dev libopenmpi-dev libcgal-dev build-essential flex bison cmake zlib1g-dev qt4-dev-tools libqt4-dev gnuplot libreadline-dev libncurses-dev libxt-dev openssh-client openssh-server linux-source linux-headers-generic linux-image texlive-full eclipse-cdt python-pypdf chromium-browser kscreensaver kate kwrite netbeans eclipse scons libfreetype6-dev libpng-dev git git-core valgrind gdb flex bison rpm build-essential zlib1g-dev binutils-dev openmpi-bin libopenmpi-dev paraview libscotch-dev cmake libstdc++5 python python-matplotlib python-numpy python-numpy-dev libboost-all-dev python-pytest python-pytest-xdist python-pip python-scipy firefox

sudo pip install numdifftools
sudo pip install pytest-cov
sudo pip install tabulate

sudo apt-get -y autoremove
sudo apt-get -y autoclean
