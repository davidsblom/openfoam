#!/bin/bash

set -e
set -x

VERSION=3.2
METIS_VERSION=5.1.0
PARMETIS_VERSION=4.0.3
PETSC_VERSION=3.6.4
DEALII_VERSION=8.3.0
BOOST_VERSION=1_61_0
BOOST_VERSION_DOT=1.61.0

cd ..

mkdir -p foam-extend-${VERSION}/ThirdParty/rpmBuild/SOURCES/

cd foam-extend-${VERSION}/ThirdParty/rpmBuild/SOURCES/

wget --no-check-certificate http://www.cmake.org/files/v3.2/cmake-3.2.2.tar.gz
wget --no-check-certificate http://downloads.sourceforge.net/project/openfoam-extend/foam-extend-3.1/ThirdParty/metis-5.1.0.tar.gz
wget --no-check-certificate http://downloads.sourceforge.net/project/openfoam-extend/foam-extend-3.1/ThirdParty/ParMGridGen-1.0.tar.gz
wget --no-check-certificate http://downloads.sourceforge.net/project/openfoam-extend/foam-extend-3.1/ThirdParty/mesquite-2.1.2.tar.gz
wget --no-check-certificate http://downloads.sourceforge.net/project/openfoam-extend/foam-extend-3.1/ThirdParty/scotch-6.0.0.tar.gz
wget --no-check-certificate http://downloads.sourceforge.net/project/openfoam-extend/foam-extend-3.1/ThirdParty/scotch_6.0.4.tar.gz
wget --no-check-certificate http://glaros.dtc.umn.edu/gkhome/fetch/sw/parmetis/parmetis-4.0.3.tar.gz
wget --no-check-certificate http://www.cs.sandia.gov/~kddevin/Zoltan_Distributions/zoltan_distrib_v3.6.tar.gz
wget --no-check-certificate http://openfoamwiki.net/images/c/cb/PyFoam-0.6.3.tar.gz
wget --no-check-certificate http://openfoamwiki.net/images/3/3b/PyFoam-0.6.4.tar.gz
wget --no-check-certificate http://downloads.sourceforge.net/project/openfoam-extend/foam-extend-3.1/ThirdParty/hwloc-1.7.2.tar.gz
wget --no-check-certificate http://downloads.sourceforge.net/project/openfoam-extend/foam-extend-3.1/ThirdParty/hwloc-1.10.1.tar.gz

# Download packages for FSI Solver
cd ../../../../davidblom-3.1-ext/src/thirdParty/
wget -O gtest-1.8.0.tar.gz https://github.com/google/googletest/archive/release-1.8.0.tar.gz
wget -O yaml-cpp-0.5.3.tar.gz https://github.com/jbeder/yaml-cpp/archive/release-0.5.3.tar.gz
wget -O boost_${BOOST_VERSION}.tar.bz2 http://downloads.sourceforge.net/project/boost/boost/${BOOST_VERSION_DOT}/boost_${BOOST_VERSION}.tar.bz2
wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-${METIS_VERSION}.tar.gz
wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/parmetis/parmetis-${PARMETIS_VERSION}.tar.gz
wget http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-${PETSC_VERSION}.tar.gz
wget https://github.com/dealii/dealii/releases/download/v${DEALII_VERSION}/dealii-${DEALII_VERSION}.tar.gz
