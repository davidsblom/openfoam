#!/bin/bash

export VERSION=3.2

set -e

cd ..

# Setting environment variables for alternative installation location

export FOAM_INST_DIR=`pwd`
export WM_THIRD_PARTY_DIR=$FOAM_INST_DIR/foam-extend-${VERSION}/ThirdParty

rm -rf ~/foam

export PARAVIEW_SYSTEM=1
export SWAK4FOAM_SYSTEM=1
export CUDA_IGNORE=1

# System install OpenMPI
export OMPI_CC=/opt/local/bin/gcc
export OMPI_CXX=/opt/local/bin/g++
export WM_CC=/opt/local/bin/gcc
export WM_CXX=/opt/local/bin/g++
export WM_MPLIB=SYSTEMOPENMPI
export OPENMPI_DIR=/usr
export OPENMPI_BIN_DIR=$OPENMPI_DIR/bin

export SCOTCH_SYSTEM=1
export SCOTCH_DIR=/opt/local
export SCOTCH_BIN_DIR=$SCOTCH_DIR/bin
export SCOTCH_LIB_DIR=$SCOTCH_DIR/lib
export SCOTCH_INCLUDE_DIR=$SCOTCH_DIR/include

# System installed Metis
export METIS_SYSTEM=1
export METIS_DIR=/opt/local
export METIS_BIN_DIR=$METIS_DIR/bin
export METIS_LIB_DIR=$METIS_DIR/lib
export METIS_INCLUDE_DIR=$METIS_DIR/include

# System installed hwloc
export HWLOC_SYSTEM=1
export HWLOC_DIR=/opt/local
export HWLOC_BIN_DIR=$HWLOC_SYSTEM/bin
export HWLOC_LIB_DIR=$HWLOC_SYSTEM/lib
export HWLOC_INCLUDE_DIR=$HWLOC_SYSTEM/include

# System installed ParMetis
export PARMETIS_SYSTEM=1
export PARMETIS_DIR=/opt/local
export PARMETIS_BIN_DIR=$PARMETIS_DIR/bin
export PARMETIS_LIB_DIR=$PARMETIS_DIR/lib
export PARMETIS_INCLUDE_DIR=$PARMETIS_DIR/include

export CMAKE_SYSTEM=1
export CMAKE_DIR=/opt/local
export CMAKE_BIN_DIR=$CMAKE_DIR/bin
export CMAKE_LIB_DIR=$CMAKE_DIR/lib
export CMAKE_INCLUDE_DIR=$CMAKE_DIR/include

ulimit -n 1024

# Clean previous compilation files

cd $FOAM_INST_DIR/foam-extend-${VERSION}

gsed -i 's/$(GFLAGS)/$(GFLAGS) -P/g' wmake/rules/darwinIntel64Gcc/general
gsed -i 's/CC          = $(WM_CXX) $(WM_CXXFLAGS)/CC          = OMPI_CXX=\/opt\/local\/bin\/g++ mpicxx -m64'/g wmake/rules/darwinIntel64Gcc/c++

source etc/bashrc
unset WM_THIRD_PARTY_USE_CMAKE_322
export WM_MPLIB=SYSTEMOPENMPI
export OPENMPI_DIR=/usr
export OPENMPI_BIN_DIR=$OPENMPI_DIR/bin
export OMPI_CC=/opt/local/bin/gcc
export OMPI_CXX=/opt/local/bin/g++
export WM_CC=/opt/local/bin/gcc
export WM_CXX=/opt/local/bin/g++

# Compile OpenFOAM extend

mpicc --version
mpicxx --version
mpirun --version
g++ --version
gcc --version

# This next command will take a while... somewhere between 30 minutes to 3-6 hours.
./Allwmake.firstInstall > >(tee make_openfoam.log) 2> >(tee make_openfoam_error.log >&2)

# Run it a second time for getting a summary of the installation
./Allwmake > >(tee make_openfoam.log) 2> >(tee make_openfoam_error.log >&2)

export OMPI_CC=/opt/local/bin/gcc
export OMPI_CXX=/opt/local/bin/g++
export WM_CC=/opt/local/bin/gcc
export WM_CXX=/opt/local/bin/g++
source etc/bashrc
export OMPI_CC=/opt/local/bin/gcc
export OMPI_CXX=/opt/local/bin/g++
export WM_CC=/opt/local/bin/gcc
export WM_CXX=/opt/local/bin/g++

# Check if icoFoam is working
icoFoam -help

# Compile all the personal solvers

cd $FOAM_INST_DIR/davidblom-3.1-ext

./Allwmake.firstInstall

fsiFoam -help
fsiFluidFoam -help
