
/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

Class
    dummy

\*---------------------------------------------------------------------------*/
#include "dummy.H"
#include "addToRunTimeSelectionTable.H"

using namespace Foam;

defineTypeNameAndDebug(dummy, 0);
addToRunTimeSelectionTable(BodyMotion, dummy, dictionary);

// * * * * * * * * * * * * * * * * Private Member Functions* * * * * * * * * //
void dummy::writePosition(const scalar& time)
{
    ofBody_ << time << "\t" << 0.0 << "\t" << 0.0 << "\t" << 0.0 << endl;
}
// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

dummy::dummy(const dictionary& dict,const polyMesh& mesh):
BodyMotion(dict.parent()),
dict_(dict),
mesh_(mesh),
ofBody_("bodyMotion.dat")
{
	Info << "BodyProperties: dummy" << endl;

    if(writeToFile())
    {
        ofBody_ << "time\tposition\tvelocity\tacceleration" << endl;
    }
    else
    {
        rm("bodyMotion.dat");
    }
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

dummy::~dummy()
{

}

// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

vector dummy::calculatePosition(const vectorField& forces)
{
    return vector::zero;
}

vector dummy::calculateSteadyStatePosition(const vectorField& forces)
{
    return vector::zero;
}

void dummy::update()
{

}

void dummy::calculateInitialPosition(const vectorField& forces)
{
    
}

void dummy::setInitialPosition(const vector& position)
{
    
}

vectorField dummy::getMotion(const vectorField& movingPoints) const
{
    return vectorField(mesh_.boundaryMesh()[patchID()].points().size(),vector::zero);
}

void dummy::write()
{
    if(writeToFile())
    {
        writePosition(mesh_.time().value());    
    }
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
