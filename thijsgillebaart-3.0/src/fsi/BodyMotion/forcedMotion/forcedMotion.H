/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    forcedMotion

Description
	Rigid body motion based sinus functions

Author
    Thijs Gillebaart, TU Delft.  All rights reserved.

SourceFiles
    forcedMotion.C

\*---------------------------------------------------------------------------*/

#ifndef forcedMotion_H
#define forcedMotion_H

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "BodyMotion.H"
#include "dimensionedVector.H"
#include "dimensionedScalar.H"
#include "Time.H"
//#include "dictionary.H"
//#include "vector.H"
//#include "vectorField.H"

namespace Foam
{

/*---------------------------------------------------------------------------*\
                      Class forcedMotion Declaration
\*---------------------------------------------------------------------------*/

class forcedMotion
:
	public BodyMotion
{
		// Private data
		const dictionary& dict_;
		const polyMesh& mesh_;

		//Body properties
		scalar frequency_;
		scalar amplitude_;
	    
		//Body movement parameters
		dimensionedVector bodyRC_;
		dimensionedVector bodyRCprevIter_;

        //Writing properties
        OFstream ofBody_;

        //- Writing function
        void writePosition(const scalar& time);

    	//- Disallow default bitwise copy construct
        forcedMotion(const forcedMotion&);
	    
	public:

    //- Runtime type information
    TypeName("forcedMotion");

	//Public data

    // Constructors

        //- Construct from dict + mesh
		forcedMotion(const dictionary& dict,const polyMesh& mesh);

		virtual autoPtr<BodyMotion> clone() const
        {
            return autoPtr<BodyMotion>(new forcedMotion(this->dict_,this->mesh_));
        };

    // Destructor

        virtual ~forcedMotion();

    // Member Functions

        //- Calculate position based on forces
        virtual vector calculatePosition(const vectorField& forces);

        //- Calculate steady state position based on forces
        virtual vector calculateSteadyStatePosition(const vectorField& forces);

        //- Calculate initial position based on forces
        virtual void calculateInitialPosition(const vectorField& forces);

        //- Set initial position
        virtual void setInitialPosition(const vector& position);

        //- Function called when new time step is entered
        virtual void update();

        //- Return difference between bodyRC_ and bodyRCprevIter_
        virtual vectorField getMotion(const vectorField& movingPoints) const;

        //- Write out body properties if requested
        virtual void write();
    
};
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
