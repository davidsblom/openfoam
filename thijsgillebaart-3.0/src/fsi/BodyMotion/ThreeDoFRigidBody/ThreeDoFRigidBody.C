
/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

Class
    ThreeDoFRigidBody

\*---------------------------------------------------------------------------*/
#include "ThreeDoFRigidBody.H"
#include "addToRunTimeSelectionTable.H"
#include "simpleMatrix.H"
#include "mathematicalConstants.H"

using namespace Foam;

defineTypeNameAndDebug(ThreeDoFRigidBody, 0);
addToRunTimeSelectionTable(BodyMotion, ThreeDoFRigidBody, dictionary);

// * * * * * * * * * * * * * * * * Private Member Functions* * * * * * * * * //
void ThreeDoFRigidBody::writePosition(const scalar& time)
{
	//Body
    ofBody_ << time << "\t" << bodyState_[0] << "\t" << bodyState_[1] << "\t" << bodyState_[2] 
            << "\t" << bodyState_[3] << "\t" << bodyState_[4] << "\t" << bodyState_[5] << endl;

    //Forces
    ofBodyForces_ << time << "\t" << bodyForces_[0] << "\t" << bodyForces_[1] << "\t" << bodyForces_[2] << endl;
}

vector ThreeDoFRigidBody::calculateForcesAndMoments(const vectorField& forces) const
{
    vector forcesAndMoments(0,0,0);
    scalar xbody = bodyState_[0];
    scalar ybody = bodyState_[1];
    vector CofR = rc_ + vector(xbody,ybody,0);
    vectorField Md = mesh_.boundaryMesh()[patchID()].faceCentres() - CofR;
    vectorField moments = (Md ^ forces);

    vector totalMoments = gSum(moments);
    vector totalForces = gSum(forces);

    Info << "totalForces = " << totalForces << endl;
    Info << "totalMoments = " << totalMoments << endl;

    forcesAndMoments.x()=totalForces.x();
    forcesAndMoments.y()=totalForces.y();
    forcesAndMoments.z()=totalMoments.z();

    return forcesAndMoments;
}

void ThreeDoFRigidBody::readInitialStateIfPresent()
{
    vector state = dict_.lookupOrDefault("initialState",vector::zero);
    vector stateVel = dict_.lookupOrDefault("initialStateVelocity",vector::zero);

    bodyState_[0] = state.x();
    bodyState_[1] = state.y();
    bodyState_[2] = state.z();
    bodyState_[3] = stateVel.x();
    bodyState_[4] = stateVel.y();
    bodyState_[5] = stateVel.z();
    theta0_ = bodyState_[2]; //needed in rotation tensors
    bodyState0_ = bodyState_;

    Info << "Initial state is set to: " << bodyState_ << endl;
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

ThreeDoFRigidBody::ThreeDoFRigidBody(const dictionary& dict,const polyMesh& mesh):
BodyMotion(dict.parent(),mesh),
dict_(dict),
mesh_(mesh),
integrationScheme_(dict_.lookup("integrationScheme")),
k_(dict_.lookup("k")),
c_(dict_.lookup("c")),
ktheta_(readScalar(dict_.lookup("ktheta"))),
ctheta_(readScalar(dict_.lookup("ctheta"))),
m_(0.0),
Icg_(readScalar(dict_.lookup("Icg"))),
Lcg2rc_(readScalar(dict_.lookup("Lcg2rc"))),
rc_(dict_.lookup("rc")),
thetaGeom_(readScalar(dict_.lookup("thetaGeom"))),
bodyState_(6,0.0),
bodyStateVel_(6,0.0),
bodyStatePrevIter_(6,0.0),
bodyStateOld_(6,0.0),
bodyState0_(6,0.0),
theta0_(0),
bodyForces_(vector::zero),
ofBody_("bodyMotion.dat"),
ofBodyForces_("bodyForces.dat")
{
	//Determine thickness of mesh and set m accordingly
	Vector<label> e = (-mesh.geometricD()+Vector<label>(1,1,1))/2;
	scalar meshDepth(vector(e.x(),e.y(),e.z()) & vector(mesh.bounds().span()));
    scalar m(readScalar(dict_.lookup("m")));
	m_ = m*meshDepth;

    //Read initial state
    readInitialStateIfPresent();

    if(writeToFile())
    {
        ofBody_ << "time\tx\ty\ttheta\tdxdt\tdydt\tdthetadt" << endl;
        ofBodyForces_ << "time\tFx\tFy\tMtheta" << endl;
    }
    else
    {
        rm("bodyMotion.dat");
    }
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

ThreeDoFRigidBody::~ThreeDoFRigidBody()
{

}

// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

vector ThreeDoFRigidBody::calculatePosition(const vectorField& forces)
{
    Info << "Solve Solid" << endl;
    const scalar& dt = mesh_.time().deltaT().value();

    vector forcesAndMoments = calculateForcesAndMoments(forces);
	bodyForces_ = forcesAndMoments;
    //Info << "(Fx,Fy,Mz) = " << forcesAndMoments << endl;

    //Start by saving previous iteration
    bodyStatePrevIter_ = bodyState_;

    //Set values of x,y,theta,dxdt,dydt,dthetadt to old values initially (every subiteration of fsi also)
    scalar xbody = bodyStateOld_[0];
    scalar ybody = bodyStateOld_[1];
    scalar theta = bodyStateOld_[2];
    scalar dxbody = bodyStateOld_[3];
    scalar dybody = bodyStateOld_[4];
    scalar dtheta = bodyStateOld_[5];

    if(integrationScheme_=="explicit")
    {
        Info << "Using explicit body motion calculation " << endl;

        simpleMatrix<scalar> C(3,0,0);
        C[0][0] = m_;
        C[0][2] = -m_*Lcg2rc_*sin(theta+thetaGeom_);
        C[1][1] = m_;
        C[1][2] = m_*Lcg2rc_*cos(theta+thetaGeom_);
        C[2][0] = -m_*Lcg2rc_*sin(theta+thetaGeom_);
        C[2][1] = m_*Lcg2rc_*cos(theta+thetaGeom_);
        C[2][2] = Icg_ + m_*pow(Lcg2rc_,2);

        C.source()[0] = forcesAndMoments.x() - c_.x()*dxbody - k_.x()*xbody + m_*Lcg2rc_*pow(dtheta,2)*cos(theta+thetaGeom_);
        C.source()[1] = forcesAndMoments.y() - c_.y()*dybody - k_.y()*ybody + m_*Lcg2rc_*pow(dtheta,2)*sin(theta+thetaGeom_);
        C.source()[2] = forcesAndMoments.z() - ctheta_*dtheta - ktheta_*theta;
        List<scalar> bodyStateAcc = C.LUsolve();

        bodyStateVel_[0]=dxbody;bodyStateVel_[1]=dybody;bodyStateVel_[2]=dtheta;
        bodyStateVel_[3]=bodyStateAcc[0];bodyStateVel_[4]=bodyStateAcc[1];bodyStateVel_[5]=bodyStateAcc[2];

        bodyState_ = bodyStateOld_ + dt*bodyStateVel_;
    }
    else if(integrationScheme_=="RK4")
    {
        List<scalar> rkCoeffs(4,1.0);
        rkCoeffs[0]=1.0/4.0;rkCoeffs[1]=1.0/3.0;rkCoeffs[2]=1.0/2.0;
        
        simpleMatrix<scalar> C(3,0,0);
        for(int i=0;i<rkCoeffs.size();i++){
            
            C = simpleMatrix<scalar>(3,0,0);
            C[0][0] = m_;
            C[0][2] = -m_*Lcg2rc_*sin(theta+thetaGeom_);
            C[1][1] = m_;
            C[1][2] = m_*Lcg2rc_*cos(theta+thetaGeom_);
            C[2][0] = -m_*Lcg2rc_*sin(theta+thetaGeom_);
            C[2][1] = m_*Lcg2rc_*cos(theta+thetaGeom_);
            C[2][2] = Icg_ + m_*pow(Lcg2rc_,2);

            C.source()[0] = forcesAndMoments.x() - c_.x()*dxbody - k_.x()*xbody + m_*Lcg2rc_*pow(dtheta,2)*cos(theta+thetaGeom_);
            C.source()[1] = forcesAndMoments.y() - c_.y()*dybody - k_.y()*ybody + m_*Lcg2rc_*pow(dtheta,2)*sin(theta+thetaGeom_);
            C.source()[2] = forcesAndMoments.z() - ctheta_*dtheta - ktheta_*theta;//Add some moment here
            List<scalar> bodyStateAcc = C.LUsolve();

            bodyStateVel_[0]=dxbody;bodyStateVel_[1]=dybody;bodyStateVel_[2]=dtheta;
            bodyStateVel_[3]=bodyStateAcc[0];bodyStateVel_[4]=bodyStateAcc[1];bodyStateVel_[5]=bodyStateAcc[2];

            List<scalar> bodyStageState = bodyStateOld_ + rkCoeffs[i]*dt*bodyStateVel_;
            xbody = bodyStageState[0];ybody = bodyStageState[1];theta = bodyStageState[2];
            dxbody = bodyStageState[3];dybody = bodyStageState[4];dtheta = bodyStageState[5];
        }

        //Info << "C = " << C << endl;
        
        bodyState_[0]=xbody;
        bodyState_[1]=ybody;
        bodyState_[2]=theta;
        bodyState_[3]=dxbody;
        bodyState_[4]=dybody;
        bodyState_[5]=dtheta;
    }
    else
    {
        FatalErrorIn("void ThreeDoFRigidBody::calculatePosition()")
            << "integrationScheme " << integrationScheme_ << " not available."
            << "Available schemes are: (explicit, RK4)"
            << abort(FatalError);
    }
    
    vector bodyStatePos(bodyState_[0],bodyState_[1],bodyState_[2]);
    //return position -> check what is done with it since z is now theta
    return bodyStatePos;
}

vector ThreeDoFRigidBody::calculateSteadyStatePosition(const vectorField& forces)
{
    vector forcesAndMoments = calculateForcesAndMoments(forces);
    bodyForces_ = forcesAndMoments;

    //Start by saving previous iteration
    bodyStatePrevIter_ = bodyState_;

    //Calculate steady state deformation
    bodyState_[0] = forcesAndMoments.x()/k_.x();
    bodyState_[1] = forcesAndMoments.y()/k_.y();
    bodyState_[2] = forcesAndMoments.z()/ktheta_;

    Info << "Bodystate: x = " << bodyState_[0] << ", y = " << bodyState_[1] << ", theta = " << bodyState_[2] << endl;
    return vector(bodyState_[0],bodyState_[1],bodyState_[2]);
}

//- Set initial position based on forces
void ThreeDoFRigidBody::calculateInitialPosition(const vectorField& forces)
{
    vector force = gSum(forces);
    
    /*bodyRC_.value().y() = force.y()/k_.value();
    bodyRCold_.value().y() = bodyRC_.value().y();
    bodyRCprevIter_.value().y() = bodyRC_.value().y();
    
    Info << "Initial displacement = " << bodyRC_.value() << endl;

    if(writeToFile())
    {
        writePosition(mesh_.time().startTime().value());
    }*/
}

void ThreeDoFRigidBody::setInitialPosition(const vector& position)
{
    /*bodyRC_.value().y() = position.y();
    bodyRCold_.value().y() = bodyRC_.value().y();
    bodyRCprevIter_.value().y() = bodyRC_.value().y();
    
    Info << "Initial displacement = " << bodyRC_.value() << endl;

    if(writeToFile())
    {
        writePosition(mesh_.time().startTime().value());
    }*/
}

void ThreeDoFRigidBody::update()
{
    bodyStateOld_ = bodyState_;
}

vectorField ThreeDoFRigidBody::getMotion(const vectorField& movingPoints) const
{
    scalar thetaCur   = bodyState_[2];
    scalar thetaPrev   = bodyStatePrevIter_[2];
    //It is assumed that rotation center (in the mesh) is located at rc_ + initial translation
    vector CofR = rc_ + vector(bodyState0_[0],bodyState0_[1],0);

    //Translation is subtracting current state with previous state
    vector translationVector(bodyState_[0] - bodyStatePrevIter_[0],bodyState_[1] - bodyStatePrevIter_[1],0);

    //Rotation
    tensor RzCur
    (
        Foam::cos(thetaCur-theta0_), -Foam::sin(thetaCur-theta0_), 0,
        Foam::sin(thetaCur-theta0_),  Foam::cos(thetaCur-theta0_), 0,
        0, 0, 1
    );

    tensor RzPrev
    (
        Foam::cos(thetaPrev-theta0_), -Foam::sin(thetaPrev-theta0_), 0,
        Foam::sin(thetaPrev-theta0_),  Foam::cos(thetaPrev-theta0_), 0,
        0, 0, 1
    );

    //Displacement due to rotation
    vectorField rotationField((RzCur-RzPrev) & (movingPoints - CofR));

    vectorField motion = translationVector + (rotationField);

    return motion;

    //vector bodyStatePosPrevIter(bodyStatePrevIter_[0],bodyStatePrevIter_[1],bodyStatePrevIter_[2]);
    //vector bodyStatePos(bodyState_[0],bodyState_[1],bodyState_[2]);
    //return bodyStatePos - bodyStatePosPrevIter;
}

void ThreeDoFRigidBody::write()
{
    if(writeToFile())
    {
        writePosition(mesh_.time().value());    
    }
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
