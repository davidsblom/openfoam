
/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

Class
    singleDirectionStiffenedBody

\*---------------------------------------------------------------------------*/
#include "singleDirectionStiffenedBody.H"
#include "addToRunTimeSelectionTable.H"

using namespace Foam;

defineTypeNameAndDebug(singleDirectionStiffenedBody, 0);
addToRunTimeSelectionTable(BodyMotion, singleDirectionStiffenedBody, dictionary);

// * * * * * * * * * * * * * * * * Private Member Functions* * * * * * * * * //
void singleDirectionStiffenedBody::writePosition(const scalar& time)
{
    ofBody_ << time << "\t" << bodyRC_.value().y() << "\t" << bodyRCvel_.value().y() << "\t" << bodyRCacc_.value().y() << endl;
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

singleDirectionStiffenedBody::singleDirectionStiffenedBody(const dictionary& dict,const polyMesh& mesh):
BodyMotion(dict.parent()),
dict_(dict),
mesh_(mesh),
integrationScheme_(dict_.lookup("integrationScheme")),
k_(dict_.lookup("k")),
m_(word("m"),dimensionSet(1,0,0,0,0,0,0),0.0),
c_(dict_.lookup("c")),
bodyRCacc_(word("bodyRCacc"),dimensionSet(0,1,-2,0,0,0,0),vector(0.0,0.0,0.0)),
bodyRCvel_(dict_.lookup("v0")),
bodyRCvelOld_(word("bodyRCvelOld"),dimensionSet(0,1,-1,0,0,0,0),vector(0.0,0.0,0.0)),
bodyRC_(dict_.lookup("x0")),
bodyRCold_(word("bodyRCold"),dimensionSet(0,1,0,0,0,0,0),vector(0.0,0.0,0.0)),
bodyRCprevIter_(word("bodyRCprevIter"),dimensionSet(0,1,0,0,0,0,0),vector(0.0,0.0,0.0)),
//writeToFile_(dict_.lookupOrDefault("writeToFile",false)),
ofBody_("bodyMotion.dat")
{
	//Determine thickness of mesh and set m accordingly
	Vector<label> e = (-mesh.geometricD()+Vector<label>(1,1,1))/2;
	dimensionedVector emptyDir("emptyDir",dimensionSet(0,0,0,0,0,0,0),vector(e.x(),e.y(),e.z()));
	dimensionedVector span("span",dimensionSet(0,1,0,0,0,0,0),mesh.bounds().span());
	dimensionedScalar meshDepth(emptyDir & span);
    dimensionedScalar m(dict_.lookup("m"));
	m_ = m*meshDepth;
	m_.name() = "m";
	
	Info << "BodyProperties: m = " << m_.value() << ", k = " << k_.value() <<", c = " << c_.value() << endl;

    if(writeToFile())
    {
        ofBody_ << "time\tposition\tvelocity\tacceleration" << endl;
    }
    else
    {
        rm("bodyMotion.dat");
    }

    Info << "x0 = " << bodyRC_ << ", v0 = " << bodyRCvel_ << endl;
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

singleDirectionStiffenedBody::~singleDirectionStiffenedBody()
{

}

// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

vector singleDirectionStiffenedBody::calculatePosition(const vectorField& forces)
{
    Info << "Solve Solid" << endl;
    const dimensionedScalar& dt(mesh_.time().deltaT());

    dimensionedVector totalForce("totalForce",dimensionSet(1,1,-2,0,0,0,0),gSum(forces));
    Info << "Total force on solid = " << totalForce << endl;

    //Start by saving previous iteration
    bodyRCprevIter_ = bodyRC_;
    bodyRCprevIter_.name() = "bodyRCprevIter";

    if(integrationScheme_=="explicit")
    {
        Info << "Using explicit body motion calculation " << endl;
        scalar theta=1.0;
        
        //Determine acceleration
        bodyRCacc_ = totalForce/m_ - k_/m_*bodyRCold_ - c_/m_*bodyRCvelOld_;
        bodyRCacc_.value().x()=0.0;//Eliminate x acc
        bodyRCacc_.value().z()=0.0;//Elminate y acc
        bodyRCacc_.name() = "bodyRCacc";
        Info << "bodyRCacc = " << bodyRCacc_ << endl;
        
        //Calculate velocity based on explicit integration
        bodyRCvel_ = bodyRCvelOld_ + dt*bodyRCacc_;
        bodyRCvel_.name() = "bodyRCvel";
        Info << "bodyRCvel = " << bodyRCvel_ << ", bodyRCvelOld = " << bodyRCvelOld_ << endl;
        
        //Calculate displacement based on integration
        bodyRC_ = bodyRCold_ + dt*((1-theta)*bodyRCvelOld_ + theta*bodyRCvel_);
        bodyRC_.name() = "bodyRC";
        Info << "bodyRCold = " << bodyRCold_.value() << ", bodyRCprevIter = " << bodyRCprevIter_.value() << endl;
    }
    else if(integrationScheme_=="RK4")
    {
        List<scalar> rkCoeffs(4,1.0);
        rkCoeffs[0]=1.0/4.0;rkCoeffs[1]=1.0/3.0;rkCoeffs[2]=1.0/2.0;
        
        List<scalar> u0(2,0.0);
        u0[0]=bodyRCold_.value().y();
        u0[1]=bodyRCvelOld_.value().y();
        List<scalar> uRK = u0;
        
        //Do RK loop for all stages
        for(int i=0;i<rkCoeffs.size();i++){
            List<scalar> uRK0 = uRK;//Save k-1 RK result
            uRK[0] = u0[0] + rkCoeffs[i]*dt.value()*uRK0[1];
            uRK[1] = u0[1] + rkCoeffs[i]*dt.value()* (totalForce.value().y()/m_.value() - k_.value()/m_.value()*uRK0[0] - c_.value()/m_.value()*uRK0[1]);
        }
        
        //Save final RK results in related variables
        bodyRC_.value().y() = uRK[0];
        bodyRCvel_.value().y() = uRK[1];
        bodyRCacc_.value().y() = totalForce.value().y()/m_.value() - k_.value()/m_.value()*uRK[0] - c_.value()/m_.value()*uRK[1];
    }
    else
    {
        FatalErrorIn("void singleDirectionStiffenedBody::calculatePosition()")
            << "integrationScheme " << integrationScheme_ << " not available."
            << "Available schemes are: (explicit, RK4)"
            << abort(FatalError);
    }
    
    Info << "BodyRC = " << bodyRC_ << endl;
    return bodyRC_.value();
}

vector singleDirectionStiffenedBody::calculateSteadyStatePosition(const vectorField& forces)
{
    Info << "Solve Solid" << endl;
    dimensionedVector totalForce("totalForce",dimensionSet(1,1,-2,0,0,0,0),gSum(forces));
    Info << "Total force on solid = " << totalForce << endl;

    //Start by saving previous iteration
    bodyRCprevIter_ = bodyRC_;
    bodyRCprevIter_.name() = "bodyRCprevIter";
    
    bodyRC_.value().y() = totalForce.value().y()/k_.value();
    bodyRCvel_.value().y() = 0.0;
    bodyRCacc_.value().y() = 0.0;

    Info << "BodyRC = " << bodyRC_ << endl;
    return bodyRC_.value();
}

//- Set initial position based on forces
void singleDirectionStiffenedBody::calculateInitialPosition(const vectorField& forces)
{
    vector force = gSum(forces);
    
    bodyRC_.value().y() = force.y()/k_.value();
    bodyRCold_.value().y() = bodyRC_.value().y();
    bodyRCprevIter_.value().y() = bodyRC_.value().y();
    
    Info << "Initial displacement = " << bodyRC_.value() << endl;

    if(writeToFile())
    {
        writePosition(mesh_.time().startTime().value());
    }
}

void singleDirectionStiffenedBody::setInitialPosition(const vector& position)
{
    bodyRC_.value().y() = position.y();
    bodyRCold_.value().y() = bodyRC_.value().y();
    bodyRCprevIter_.value().y() = bodyRC_.value().y();
    
    Info << "Initial displacement = " << bodyRC_.value() << endl;

    if(writeToFile())
    {
        writePosition(mesh_.time().startTime().value());
    }
}

void singleDirectionStiffenedBody::update()
{
    bodyRCold_ = bodyRC_;
    bodyRCold_.name() = "bodyRCold";
    bodyRCvelOld_ = bodyRCvel_;
    bodyRCvelOld_.name() = "bodyRCvelOld";
}

vectorField singleDirectionStiffenedBody::getMotion(const vectorField& movingPoints) const
{
    vectorField motion(mesh_.boundaryMesh()[patchID()].points().size(),vector::zero);
    motion = bodyRC_.value()-bodyRCprevIter_.value();
    return motion;
}

void singleDirectionStiffenedBody::write()
{
    if(writeToFile())
    {
        writePosition(mesh_.time().value());    
    }
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
