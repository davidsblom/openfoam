/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    singleDirectionStiffenedBody

Description
	Rigid body motion based sinus functions

Author
    Thijs Gillebaart, TU Delft.  All rights reserved.

SourceFiles
    singleDirectionStiffenedBody.C

\*---------------------------------------------------------------------------*/

#ifndef singleDirectionStiffenedBody_H
#define singleDirectionStiffenedBody_H

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "bodyMotionFunction.H"
#include "dimensionedVector.H"
#include "dimensionedScalar.H"
#include "Time.H"

namespace Foam
{

/*---------------------------------------------------------------------------*\
                      Class singleDirectionStiffenedBody Declaration
\*---------------------------------------------------------------------------*/

class singleDirectionStiffenedBody
:
    public bodyMotionFunction
{
	// Private data
	const dictionary& dict_;
	const polyMesh& mesh_;

    //Solve properties
    word integrationScheme_;

	//Body properties
	dimensionedScalar k_;
	dimensionedScalar m_;
    dimensionedScalar c_;
    
	//Body movement parameters
	dimensionedVector bodyRCacc_;
	dimensionedVector bodyRCvel_;
	dimensionedVector bodyRCvelOld_;
	dimensionedVector bodyRC_;
	dimensionedVector bodyRCold_;
	dimensionedVector bodyRCprevIter_;

    //Writing properties
    OFstream ofBody_;

    // Private Member Functions

        //- Writing function
        void writePosition(const scalar& time);

    	//- Disallow default bitwise copy construct
        singleDirectionStiffenedBody(const singleDirectionStiffenedBody&);

    protected:

        virtual tmp<Field<vectorField> > getChildMotion();

        virtual tmp<Field<vectorField> > getChildMotion(const Field<vectorField>& patchForces);

        void setInitialPoints();

        virtual const labelList getChildMotionPatchIDs() const;

        virtual const wordList getChildMotionPatchNames() const;
	    
	public:

    //- Runtime type information
    TypeName("singleDirectionStiffenedBody");

	//Public data

    // Constructors

        //- Construct from dict + mesh
		singleDirectionStiffenedBody(const dictionary& dict,const polyMesh& mesh);

		virtual autoPtr<BodyMotion> clone() const
        {
            return autoPtr<BodyMotion>(new singleDirectionStiffenedBody(this->dict_,this->mesh_));
        };

    // Destructor

        virtual ~singleDirectionStiffenedBody();

    // Member Functions

        //- Set position of wing based on time
        virtual const Field<vectorField>& getPosition() const = 0;

        virtual const Field<vectorField>& getOldPosition() const = 0;

        //- Calculate position based on forces
        virtual vector calculatePosition(const vectorField& forces);

        //- Calculate steady state position based on forces
        virtual vector calculateSteadyStatePosition(const vectorField& forces);

        //- Calculate initial position based on forces
        virtual void calculateInitialPosition(const vectorField& forces);

        //- Set initial position
        virtual void setInitialPosition(const vector& position);

        //- Function called when new time step is entered
        virtual void update();

        //- Return difference between bodyRC_ and bodyRCprevIter_
        virtual vectorField getMotion(const vectorField& movingPoints) const;

        //- Write out body properties if requested
        virtual void write();
    
};
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
