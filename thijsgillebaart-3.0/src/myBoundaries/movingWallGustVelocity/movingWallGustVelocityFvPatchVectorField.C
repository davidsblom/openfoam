/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

\*---------------------------------------------------------------------------*/

#include "movingWallGustVelocityFvPatchVectorField.H"
#include "addToRunTimeSelectionTable.H"
#include "volFields.H"
#include "surfaceFields.H"
#include "fvcMeshPhi.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

movingWallGustVelocityFvPatchVectorField::movingWallGustVelocityFvPatchVectorField
(
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF
)
:
    fixedValueFvPatchVectorField(p, iF)
{}


movingWallGustVelocityFvPatchVectorField::movingWallGustVelocityFvPatchVectorField
(
    const movingWallGustVelocityFvPatchVectorField& ptf,
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF,
    const fvPatchFieldMapper& mapper
)
:
    fixedValueFvPatchVectorField(ptf, p, iF, mapper)
{}


movingWallGustVelocityFvPatchVectorField::movingWallGustVelocityFvPatchVectorField
(
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF,
    const dictionary& dict
)
:
    fixedValueFvPatchVectorField(p, iF)
{
    fvPatchVectorField::operator=(vectorField("value", dict, p.size()));
}


movingWallGustVelocityFvPatchVectorField::movingWallGustVelocityFvPatchVectorField
(
    const movingWallGustVelocityFvPatchVectorField& pivpvf
)
:
    fixedValueFvPatchVectorField(pivpvf)
{}


movingWallGustVelocityFvPatchVectorField::movingWallGustVelocityFvPatchVectorField
(
    const movingWallGustVelocityFvPatchVectorField& pivpvf,
    const DimensionedField<vector, volMesh>& iF
)
:
    fixedValueFvPatchVectorField(pivpvf, iF)
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void movingWallGustVelocityFvPatchVectorField::updateCoeffs()
{
    if (updated())
    {
        return;
    }

    const fvPatch& p = patch();
    const polyPatch& pp = p.patch();
    const fvMesh& mesh = dimensionedInternalField().mesh();
    const pointField& oldPoints = mesh.oldPoints();

    vectorField oldFc(pp.size());

    forAll(oldFc, i)
    {
        oldFc[i] = pp[i].centre(oldPoints);
    }

    vectorField Up = (pp.faceCentres() - oldFc)/mesh.time().deltaT().value();

    const volVectorField& U =
        mesh.lookupObject<volVectorField>(dimensionedInternalField().name());        

    const surfaceVectorField& Ugust =
        mesh.lookupObject<surfaceVectorField>(dimensionedInternalField().name()+"gust");
    const vectorField UgustPatch = p.patchField<surfaceVectorField, vector>(Ugust);

    scalarField phip =
        p.patchField<surfaceScalarField, scalar>(fvc::meshPhi(U));

    vectorField n = p.nf();
    const scalarField& magSf = p.magSf();
    scalarField Un = phip/(magSf + VSMALL);


/*    scalarField mphip = p.patchField<surfaceScalarField, scalar>(mesh.phi());
    scalarField mphip0 = p.patchField<surfaceScalarField, scalar>(mesh.phi().oldTime());
    vectorField Upatch = U.internalField();
    vectorField Upatch0 = U.oldTime().internalField();

    Info << "Upatch = " << Upatch[0] << endl;
    Info << "Uptach0 = " << Upatch0[0] << endl;
    Info << "mesh.phi() = " << mphip[0] << endl;
    Info << "mesh.phi().oldTime()  = " << mphip0[0] << endl;
    Info << "phip = " << phip[0] << endl;
    Info << "Un = " << Un[0] << endl;
    Info << "n & Up = " << (n & Up)()[0] << endl; */

    vectorField::operator=(-UgustPatch + Up + n*(Un - (n & Up)));
    fixedValueFvPatchVectorField::updateCoeffs();
}


void movingWallGustVelocityFvPatchVectorField::write(Ostream& os) const
{
    fvPatchVectorField::write(os);
    writeEntry("value", os);
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

makePatchTypeField
(
    fvPatchVectorField,
    movingWallGustVelocityFvPatchVectorField
);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //
