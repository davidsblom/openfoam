    Info << "Reading coupling properties" << endl;

    IOdictionary couplingProperties
    (
        IOobject
        (
            "couplingProperties",
            runTime.constant(),
            mesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

	// === Setting up interface properties === //
	fsiInterface fsiInter(mesh,couplingProperties);
	fsiInter.initializeForce(rhoFluid,nu,U,p);
        
    // === Setting up fluid mesh motionSolver === //
    RBFMotionSolverRigid& ms =
        const_cast<RBFMotionSolverRigid&>
        (
            mesh.lookupObject<RBFMotionSolverRigid>("dynamicMeshDict")
        );

    const vectorField& movingPoints = ms.movingPoints();
