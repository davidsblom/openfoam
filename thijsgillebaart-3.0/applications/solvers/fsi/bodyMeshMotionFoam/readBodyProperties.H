    Info<< "Reading solid body properties\n" << endl;

    IOdictionary bodyProperties
    (
        IOobject
        (
            "bodyProperties",
            runTime.constant(),
            mesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

    // === BodyMotion object === //
    word bmType = bodyProperties.lookup("bodyMotion");
    autoPtr<BodyMotion> bmPtr
    (
        BodyMotion::New
        (
            bmType,
    		bodyProperties,
			mesh
        )
    );

    BodyMotion& bodyMotion = bmPtr();

    word patchName(bodyProperties.lookup("bodyPatch"));
    label patchID = mesh.boundaryMesh().findPatchID(patchName);
    if(patchID<0){
        FatalErrorIn("BodyMotion::BodyMotion(const dictionary& dict,const polyMesh& mesh)")
            << "Patch " << patchName << " not found.  "
            << "valid patch names: " << mesh.boundaryMesh().names()
            << abort(FatalError);
    }

    const polyPatch& bodyPatch = mesh.boundaryMesh()[patchID];

    // === Setting up fluid mesh motionSolver === //
    RBFMotionSolverRigid& ms =
        const_cast<RBFMotionSolverRigid&>
        (
            mesh.lookupObject<RBFMotionSolverRigid>("dynamicMeshDict")
        );

    const vectorField& movingPoints = ms.movingPoints();