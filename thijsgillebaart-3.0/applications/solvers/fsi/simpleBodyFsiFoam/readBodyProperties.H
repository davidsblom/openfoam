    Info<< "Reading solid body properties\n" << endl;

    IOdictionary bodyProperties
    (
        IOobject
        (
            "bodyProperties",
            runTime.constant(),
            mesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

    // === BodyMotion object === //
    word bmType = bodyProperties.lookup("bodyMotion");
    autoPtr<BodyMotion> bmPtr
    (
        BodyMotion::New
        (
            bmType,
    		bodyProperties,
			mesh
        )
    );

    BodyMotion& bodyMotion = bmPtr();