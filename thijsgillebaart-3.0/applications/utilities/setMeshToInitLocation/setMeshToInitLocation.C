/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Application
    setMeshToInitLocation

Description
	For RBFMotionSolverFiles the initial mesh might not be in the right shape.
	This utility moves the original mesh to the initial location (file with t=0).
	Executing this utility several times will displace the mesh further with the
	same deformation vector. Only execute one time!!!

\*---------------------------------------------------------------------------*/

#include "argList.H"
#include "Time.H"
#include "RBFMotionSolverFiles.H"
#include "dynamicFvMesh.H"

using namespace Foam;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
//  Main program:

int main(int argc, char *argv[])
{
#   include "setRootCase.H"
#   include "createTime.H"
#	include "createMesh.H"

	Info << "Read mesh from " << mesh.pointsInstance() << endl <<endl;
	
	IOdictionary dynDict
	(
	   	IOobject
	   	(
		    "dynamicMeshDict",
		    mesh.time().constant(),
		    mesh,
		    IOobject::MUST_READ,
		    IOobject::AUTO_WRITE,
		    false
	   	)
	);

	//Create RBFMotionSolver only for single deformation to mesh for t=0
	const polyMesh& msMesh(mesh);
    Istream& msData = dynDict.lookup("solver");
	RBFMotionSolverFiles ms(msMesh,msData);
	
	if(fileName(dynDict.subDict("shapeFiles").lookup("gridLocationFile")) !=fileName("none"))
	{
		// Prepare new points: same as current points
		tmp<pointField> tnewPoints
		(
		    new vectorField(mesh.nPoints(), vector::zero)
		);

		pointField& newPoints = tnewPoints();
	
		//Call function for setting initial mesh specifically created for this application
		ms.setInitialMesh(newPoints);
	
		//====================================================
		//=== Write out mesh into "0" directory ==============
		//====================================================
        
        //Get patch sizes
        const polyBoundaryMesh& bMesh = mesh.boundaryMesh();
        labelList patchSizes(bMesh.size(), 0);
        labelList patchStarts(bMesh.size(), 0);

        forAll (bMesh, patchI)
        {
            patchSizes[patchI] = bMesh[patchI].size();
            patchStarts[patchI] = bMesh[patchI].start();
        }
		
		//Reset the mesh (only points change)
		mesh.resetPrimitives
        (
        	xferMove(newPoints),//newPoints,
            Xfer<faceList>::null(),//mesh.allFaces(),
            Xfer<labelList>::null(),//mesh.faceOwner(),
            Xfer<labelList>::null(),//mesh.faceNeighbour(),
            patchSizes,
            patchStarts,
            true                // boundary forms valid boundary mesh.
        );

        // Clear the addressing
        mesh.clearOut();
		
		// Flags the mesh files as being changed
		Info << "Writing mesh into directory " << mesh.time().timeName() << nl << endl;
		mesh.setInstance(mesh.time().timeName());
		
		mesh.write();
		//====================================================
		//=== Change gridLocationFile entry in dynamicMeshDict
		//====================================================
		
		dynDict.subDict("shapeFiles").set("gridLocationFile",fileName("none"));
	
		Info << "Changing gridLocationFile entry in " << dynDict.name() << " to " << fileName("none") << endl;
		dynDict.Foam::regIOobject::write();
	}	
	else
	{
		Info << endl;
		Info << "gridLocationFile entry is set to none. This indicates that the control points are already at the interpolated location." << endl;
		Info << "If initial deformation is still desired, change entry to grid location file!" << endl;
	}
    return(0);
}


// ************************************************************************* //
